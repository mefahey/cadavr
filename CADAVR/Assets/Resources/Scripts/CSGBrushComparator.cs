﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CSGBrushComparator : MonoBehaviour {

	// Convert to namespace?
	// Rename to CSGOperations?

	// Calculations
	// Local Space?
	// Compare Planes
	// Cull Vertices

	// Compares a polygon of a brush against a single plane (presumably, but not necessarily from another brush)
	// Returns returns a boolean (true if split, false otherwise), and 
	// Intersections are 


	// Add Category Field to Polygon Class?

	// Addition     - Outside + Outside
	// Intersection - Inside  + Inside
	// Subtraction  - Outside + Inside OR Inside + Outside

	// Outside should always be normally aligned
	// Inside should always be inverse aligned

	private const float DELTA = 0.003f;

	//True  -> Split
	//False -> not Split
	public static bool splitPolygonByPlane(Polygon poly, Plane plane, out Polygon inside, out Polygon outside, out HalfEdge inseam, out HalfEdge outseam) {

		inside = new Polygon(new List<HalfEdge>(), poly.plane, poly.material);
		outside = new Polygon(new List<HalfEdge>(), poly.plane, poly.material);
		Polygon[] polys = new Polygon[2] { inside, outside };

		Vector3[] intersections = new Vector3[2];
		HalfEdge[] connections = new HalfEdge[4];

		// Connections Visualization
		//
		// 1 |  out	| 2
		//	 + ---- + 
		// 3 |  in	| 0
		//
		// 0 -> split in -> 3, 1 -> split out -> 2

		inseam = null;
		outseam = null;

		// Add flush polys to inside
		if (Mathf.Abs(Vector3.Dot(poly.plane.normal, plane.normal)) >= 1 - DELTA && Mathf.Abs(Vector3.Distance((poly.plane.distance * -poly.plane.normal), (plane.distance * - plane.normal))) < DELTA) {
			polys[0].hedges = poly.hedges;
			return false; //technically not a split
		}

		for (int i = 0; i < poly.hedges.Count; i++) {
			HalfEdge h = poly.hedges[i];

			int side = (Mathf.Abs(plane.GetDistanceToPoint(h.start)) < DELTA) ?
				((plane.GetSide(h.next.start)) ? 1 : 0) : ((plane.GetSide(h.start)) ? 1 : 0);

			h.poly = polys[side];

			if (Mathf.Abs(plane.GetDistanceToPoint(h.next.start)) < DELTA) {
				intersections[side] = h.next.start; //overwrites when approximately flush

				connections[side] = h;
				connections[side + 2] = h.next;

				polys[side].hedges.Add(h);

				//Debug.Log("on plane");
			} else if (Mathf.Abs(plane.GetDistanceToPoint(h.start)) > DELTA && !plane.SameSide(h.start, h.next.start)) {
				//DO NOT DELETE THIS CONDITION -> Mathf.Abs(plane.GetDistanceToPoint(h.start.position)) > DELTA && 
				Vector3 intersect = FindLinePlaneIntersection(h, plane);
				intersections[side] = intersect;

				HalfEdge n = new HalfEdge(intersect, polys[1 - side]);
				n.next = h.next;

				if (h.twin != null) {
					HalfEdge t = new HalfEdge(h.next.start, h.twin.poly);
					t.next = h.twin;

					n.twin = t;
					t.twin = n;

					h.twin.start = intersect;
					h.twin.poly.hedges.Insert(h.twin.poly.hedges.IndexOf(h.twin), t); //bind to twin poly
					int index = h.twin.poly.hedges.IndexOf(t);
					h.twin.poly.hedges[((index != 0) ? index : h.twin.poly.hedges.Count) - 1].next = t;
				}

				connections[side] = h;
				connections[side + 2] = n;

				polys[side].hedges.Add(h);
				side = 1 - side; //toggles between 1 and 0
				polys[side].hedges.Add(n);

				//Debug.Log("across plane");
			} else {
				polys[side].hedges.Add(h);

				//Debug.Log("no split");
			}
		}

		if (inside.hedges.Count != 0 && outside.hedges.Count != 0) {
			HalfEdge sin = new HalfEdge(intersections[0], polys[0]);
			HalfEdge sout = new HalfEdge(intersections[1], polys[1]);
			//sin.twin = sout;
			//sout.twin = sin;
			//Obslete with new faces

			connections[0].next = sin;
			sin.next = connections[3];

			connections[1].next = sout;
			sout.next = connections[2];

			inside.hedges.Insert(inside.hedges.IndexOf(sin.next), sin);
			outside.hedges.Insert(outside.hedges.IndexOf(sout.next), sout);

			//OUTPUT SIN AND SOUT?
			inseam = sin;
			outseam = sout;

			return true;
		}

		return false;
	}

	public static Vector3 FindLinePlaneIntersection(HalfEdge h, Plane p) {
		// https://en.wikipedia.org/wiki/Line%E2%80%93plane_intersection

		float scalar = Vector3.Dot((p.normal * -p.distance) - h.start, p.normal) / Vector3.Dot(h.next.start - h.start, p.normal);
		Vector3 intersect = (scalar * (h.next.start - h.start)) + h.start;

		return intersect;
	}

	// Identical to splitSurfaceByPlane, except now includes polygon and material
	public static bool splitSurfaceByPolygon(Surface surf, Polygon poly, out List<Polygon> interior, out List<Polygon> exterior) {

		interior = new List<Polygon>();
		exterior = new List<Polygon>();

		Polygon planeIn = new Polygon(new List<HalfEdge>(), poly.plane, poly.material);
		Polygon planeOut = new Polygon(new List<HalfEdge>(), poly.plane.flipped, poly.material);

		foreach (Polygon p in surf.polys) {

			Polygon inside;
			Polygon outside;

			HalfEdge inseam;
			HalfEdge outseam;

			bool split = splitPolygonByPlane(p, poly.plane, out inside, out outside, out inseam, out outseam);

			if (split) {
				interior.Add(inside);
				exterior.Add(outside);

				//Add new halfedges to planeIn.hedges, planeOut.hedges (one each)
				//Bind twins sin and sout
				HalfEdge patch;

				patch = new HalfEdge(inseam.next.start, planeIn);
				patch.twin = inseam;
				inseam.twin = patch;
				planeIn.hedges.Insert(0, patch);

				patch = new HalfEdge(outseam.next.start, planeOut);
				patch.twin = outseam;
				outseam.twin = patch;
				planeOut.hedges.Add(patch);
			} else {
				if (inside.hedges.Count != 0) {
					interior.Add(p);
				} else if (outside.hedges.Count != 0) {
					exterior.Add(p);
				}
			}
		}

		//Bind consecutive polys in planeIn, planeOut
		//Add planeIn to interior, add planeOut to exterior

		//*
		//Debug.Log(planeIn.hedges.Count + " " + planeOut.hedges.Count);

		if (planeIn.hedges.Count != 0 && planeOut.hedges.Count != 0) { //true if split, false if not split

			//Bind consecutive polys in planeIn, planeOut
			//Add planeIn to interior, add planeOut to exterior

			//NOTE: MESH HOLES ON INTERNAL FACES

			//FIX MESH GAPS ON CONSECUTIVE CUTS? (extsurfs[2])
			//ON INTERNAL FACE -> IGNORE?

			// ISSUE SEEMS TO BE WITH PLANEIN/PLANEOUT -> SEAMS/PATCHES?
			// APPARENTLY ALWAYS ON LAST POLY IN SUBSURFACE -> MAKES SENSE, PLANEIN/OUT GETS ADDED LAST
			/*
			GameObject s  = new GameObject();
			s.name = "Surface";

			GameObject sin = new GameObject();
			sin.name = "Inside";
			sin.transform.SetParent(s.transform);
			foreach (Polygon p in interior) {
				GameObject pin = new GameObject();
				pin.name = "Poly";
				pin.transform.SetParent(sin.transform);
				foreach (HalfEdge h in p.hedges) {
					GameObject he = GameObject.CreatePrimitive(PrimitiveType.Cube);
					he.name = "h";
					he.transform.position = h.start;
					he.transform.localScale *= 0.05f;
					he.transform.SetParent(pin.transform);
				}
			}

			GameObject sout = new GameObject();
			sout.name = "Outside";
			sout.transform.SetParent(s.transform);
			foreach (Polygon p in exterior) {
				GameObject pout = new GameObject();
				pout.name = "Poly";
				pout.transform.SetParent(sout.transform);
				foreach (HalfEdge h in p.hedges) {
					GameObject he = GameObject.CreatePrimitive(PrimitiveType.Cube);
					he.name = "h";
					he.transform.position = h.start;
					he.transform.localScale *= 0.05f;
					he.transform.SetParent(pout.transform);
				}
			}
			//*/

			// INCONSISTENCY BETWEEN IN AND OUT
			// EXCESS VERTICES -> SEEMINGLY ALWAYS AT START OR END OF LIST
			/*
			GameObject pin = new GameObject();
			pin.name = "PlaneIn";
			foreach (HalfEdge h in planeIn.hedges) {
				GameObject he = GameObject.CreatePrimitive(PrimitiveType.Cube);
				he.name = "h";
				he.transform.position = h.start;
				he.transform.localScale *= 0.05f;
				he.GetComponent<MeshRenderer>().material.SetColor("Color", Color.magenta);
				he.transform.SetParent(pin.transform);
			}

			GameObject pout = new GameObject();
			pout.name = "PlaneOut";
			foreach (HalfEdge h in planeOut.hedges) {
				GameObject he = GameObject.CreatePrimitive(PrimitiveType.Cube);
				he.name = "h";
				he.transform.position = h.start;
				he.transform.localScale *= 0.05f;
				he.GetComponent<MeshRenderer>().material.SetColor("Color", Color.magenta);
				he.transform.SetParent(pout.transform);
			}
			//*/

			//NOTE: ERROR ALMOST CERTAINLY CAUSED BY SORTHALFEDGES

			//Executed twice to correct any errors from first pass (known issue with certain polys)
			planeIn.hedges = CSGBrushFactory.SortHalfEdges(planeIn.hedges, planeIn);
			//planeIn.hedges = CSGBrushFactory.SortHalfEdges(planeIn.hedges, planeIn);
			planeOut.hedges = CSGBrushFactory.SortHalfEdges(planeOut.hedges, planeOut);
			//planeOut.hedges = CSGBrushFactory.SortHalfEdges(planeOut.hedges, planeOut);

			CSGBrushFactory.BindSerialHalfEdges(new List<Polygon> { planeIn, planeOut });

			//Comment these out to revert
			interior.Add(planeIn);
			exterior.Add(planeOut);

			// ISSUE SEEMS TO BE WITH PLANEIN/PLANEOUT
			// NOTE: INCORRECT SORTING CAUSES HALFEDGES TO BE CULLED INCORRECTLY
			/*
			GameObject s = GameObject.CreatePrimitive(PrimitiveType.Plane);
			s.transform.position = poly.plane.normal * -poly.plane.distance;
			s.transform.up = poly.plane.normal;
			s.transform.localScale = Vector3.one * 0.25f;
			s.GetComponent<MeshRenderer>().enabled = false;
			s.name = "Surface";

			GameObject sin = new GameObject();
			sin.name = "Inside";
			sin.transform.SetParent(s.transform);
			foreach (Polygon p in interior) {
				GameObject pin = new GameObject();
				pin.name = "Poly";
				pin.transform.SetParent(sin.transform);
				foreach (HalfEdge h in p.hedges) {
					GameObject he = GameObject.CreatePrimitive(PrimitiveType.Cube);
					he.name = "h";
					he.transform.position = h.start;
					he.transform.localScale *= 0.05f;
					he.transform.SetParent(pin.transform);
				}
			}

			GameObject sout = new GameObject();
			sout.name = "Outside";
			sout.transform.SetParent(s.transform);
			foreach (Polygon p in exterior) {
				GameObject pout = new GameObject();
				pout.name = "Poly";
				pout.transform.SetParent(sout.transform);
				foreach (HalfEdge h in p.hedges) {
					GameObject he = GameObject.CreatePrimitive(PrimitiveType.Cube);
					he.name = "h";
					he.transform.position = h.start;
					he.transform.localScale *= 0.05f;
					he.transform.SetParent(pout.transform);
				}
			}
			//*/

			/*
			GameObject pin = new GameObject();
			pin.name = "PlaneIn";
			foreach (HalfEdge h in planeIn.hedges) {
				GameObject ep = new GameObject();
				ep.name = "Edge Pair";
				ep.transform.position = h.start + h.next.start;
				ep.transform.SetParent(pin.transform);

				GameObject he = GameObject.CreatePrimitive(PrimitiveType.Cube);
				he.name = "h";
				he.transform.position = h.start;
				he.transform.localScale *= 0.05f;
				he.GetComponent<MeshRenderer>().material.SetColor("Color", Color.magenta);
				he.transform.SetParent(ep.transform);

				GameObject te = GameObject.CreatePrimitive(PrimitiveType.Sphere);
				te.name = "t";
				te.transform.position = h.next.start;
				te.transform.localScale *= 0.0675f;
				te.GetComponent<MeshRenderer>().material.SetColor("Color", Color.green);
				te.transform.SetParent(ep.transform);
			}

			GameObject pout = new GameObject();
			pout.name = "PlaneOut";
			foreach (HalfEdge h in planeOut.hedges) {
				GameObject ep = new GameObject();
				ep.name = "Edge Pair";
				ep.transform.position = h.start + h.next.start;
				ep.transform.SetParent(pout.transform);

				GameObject he = GameObject.CreatePrimitive(PrimitiveType.Cube);
				he.name = "h";
				he.transform.position = h.start;
				he.transform.localScale *= 0.05f;
				he.GetComponent<MeshRenderer>().material.SetColor("Color", Color.magenta);
				he.transform.SetParent(ep.transform);

				GameObject te = GameObject.CreatePrimitive(PrimitiveType.Sphere);
				te.name = "t";
				te.transform.position = h.next.start;
				te.transform.localScale *= 0.0675f;
				te.GetComponent<MeshRenderer>().material.SetColor("Color", Color.green);
				te.transform.SetParent(ep.transform);
			}
			//*/

			return true;
		}
		return false;
		//*/
	}

	//Split one surface by another
	//Accomodates translation and rotation
	public static bool splitSurfaceBySurface(Surface surface, Surface template, Vector3 relativeTranslation, Quaternion relativeRotation, Vector3 relativeScale, out Surface inside, out List<Surface> outside) { //Cut by surface by Surface or Surface by Multiple Surfaces/Brush? (Surf for testing)

		outside = new List<Surface>();

		Surface surf = new Surface(DuplicateInstances(surface.polys));
		foreach (Polygon p in template.polys) {

			List<Polygon> interior;
			List<Polygon> exterior;

			float scaleX = Vector3.Dot(p.plane.normal, new Vector3(relativeScale.x, 0, 0));
			float scaleY = Vector3.Dot(p.plane.normal, new Vector3(0, relativeScale.y, 0));
			float scaleZ = Vector3.Dot(p.plane.normal, new Vector3(0, 0, relativeScale.z));

			float scaleXYZ = Vector3.Magnitude(new Vector3(scaleX, scaleY, scaleZ));

			Plane plane = new Plane(relativeRotation * p.plane.normal, p.plane.distance * scaleXYZ);
			plane.Translate(relativeTranslation);

			Polygon np = new Polygon(p.hedges, plane, p.material);

			/*
			GameObject g = GameObject.CreatePrimitive(PrimitiveType.Cube);
			g.transform.position = (p.plane.normal * -p.plane.distance);
			g.transform.up = p.plane.normal;
			g.transform.localScale = new Vector3(1, 0.1f, 1) * 0.1f;

			GameObject dg = GameObject.CreatePrimitive(PrimitiveType.Cube);
			dg.transform.position = (np.normal * p.plane.distance) - relativeTranslation;
			dg.transform.up = np.normal;
			dg.transform.localScale = new Vector3(1, 0.1f, 1) * 0.1f;
			//dg.transform.SetParent(g.transform);
			dg.GetComponent<MeshRenderer>().material.SetColor("_Color", Color.black);
			//*/

			splitSurfaceByPolygon(surf, np, out interior, out exterior);

			if (exterior.Count > 0) {
				outside.Add(new Surface(exterior));
			}
			surf = new Surface(DuplicateInstances(interior));
		}

		inside = surf;

		//HE MISSING HERE
		/*
		foreach (Surface s in outside) {
			GameObject e = new GameObject();
			e.name = "Surface";
			foreach (Polygon p in s.polys) {
				GameObject poly = new GameObject();
				poly.transform.SetParent(e.transform);
				poly.name = "Polygon";
				foreach (HalfEdge h in p.hedges) {
					GameObject ep = new GameObject();
					ep.name = "Edge Pair";
					ep.transform.position = h.start + h.next.start;
					ep.transform.SetParent(poly.transform);

					GameObject he = GameObject.CreatePrimitive(PrimitiveType.Cube);
					he.name = "h";
					he.transform.position = h.start;
					he.transform.localScale *= 0.05f;
					he.GetComponent<MeshRenderer>().material.SetColor("Color", Color.magenta);
					he.transform.SetParent(ep.transform);

					GameObject te = GameObject.CreatePrimitive(PrimitiveType.Sphere);
					te.name = "t";
					te.transform.position = h.next.start;
					te.transform.localScale *= 0.0675f;
					te.GetComponent<MeshRenderer>().material.SetColor("Color", Color.green);
					te.transform.SetParent(ep.transform);
				}
			}
		}

		/*
		GameObject de = new GameObject();
		de.name = "Surface";
		foreach (Polygon p in inside.polys) {
			GameObject poly = new GameObject();
			poly.transform.SetParent(de.transform);
			poly.name = "Polygon";
			foreach (HalfEdge h in p.hedges) {
				GameObject ep = new GameObject();
				ep.name = "Edge Pair";
				ep.transform.position = h.start + h.next.start;
				ep.transform.SetParent(poly.transform);

				GameObject he = GameObject.CreatePrimitive(PrimitiveType.Cube);
				he.name = "h";
				he.transform.position = h.start;
				he.transform.localScale *= 0.05f;
				he.GetComponent<MeshRenderer>().material.SetColor("Color", Color.magenta);
				he.transform.SetParent(ep.transform);

				GameObject te = GameObject.CreatePrimitive(PrimitiveType.Sphere);
				te.name = "t";
				te.transform.position = h.next.start;
				te.transform.localScale *= 0.0675f;
				te.GetComponent<MeshRenderer>().material.SetColor("Color", Color.green);
				te.transform.SetParent(ep.transform);
			}
		}

		//*/

		return (inside.polys.Count != 0 && outside.Count != 0);
	}

	//

	public static bool splitSurfaceBySurfaces(Surface surface, List<Surface> templates, 
		Vector3 relativeTranslation, Quaternion relativeRotation, Vector3 relativeScale,
		out List<Surface> interiors, out List<Surface> exteriors)
	{
		interiors = new List<Surface>();
		exteriors = new List<Surface>();

		List<Surface> surfaces = new List<Surface> { surface };
		for(int i = 0; i < templates.Count; i++) {
			List<Surface> trimmings = new List<Surface>();
			Surface t = templates[i];
			for(int j = 0; j < surfaces.Count; j++) {
				Surface s = surfaces[j];

				Surface interior;
				List<Surface> exterior;

				bool split = splitSurfaceBySurface(s, t, relativeTranslation, relativeRotation, relativeScale, out interior, out exterior);
		
				if (split) {
					interiors.Add(interior);
					trimmings.AddRange(exterior);
					surfaces.Remove(s);
				} else if  (exterior.Count == 0) {
					interiors.Add(interior);
					surfaces.Remove(s);
				}
			}
			surfaces.AddRange(trimmings);
		}
		exteriors = surfaces;
		
		return (interiors.Count != 0 && exteriors.Count != 0);
	}

	public static void splitSurfacesBySurfaces(List<Surface> surfaces, List<Surface> templates,
		Vector3 relativeTranslation, Quaternion relativeRotation, Vector3 relativeScale,
		out List<Surface> interiors, out List<Surface> exteriors) {

		interiors = new List<Surface>();
		exteriors = new List<Surface>();

		foreach(Surface s in surfaces) {

			List<Surface> interior;
			List<Surface> exterior;

			bool split = splitSurfaceBySurfaces(s, templates, relativeTranslation, relativeRotation, relativeScale, out interior, out exterior);

			interiors.AddRange(interior);
			exteriors.AddRange(exterior);
		}
	}

	//

	public static Vector3 RelativeTranslation(Transform a, Transform b) {

		Vector3 translation = Quaternion.FromToRotation(a.up, Vector3.up) * (a.position - b.position);

		// Works for proportional scaling (Vector3.one * f) only; unclear why
		translation = new Vector3(
			translation.x / a.localScale.x, 
			translation.y / a.localScale.y, 
			translation.z / a.localScale.z);

		return translation;
	}

	public static Quaternion RelativeRotation(Transform a, Transform b) {

		Quaternion rotation = Quaternion.Inverse(a.rotation) * b.rotation;

		return rotation;
	}

	public static Vector3 RelativeScale(Transform a, Transform b) {

		Vector3 scale = new Vector3(
			b.lossyScale.x / a.lossyScale.x,
			b.lossyScale.y / a.lossyScale.y,
			b.lossyScale.z / a.lossyScale.z);
		// lossyScale for absolute scale (does not work for non-uniform scaling)
		
		return scale;
	}

	//NOTE: Certain transforms cause program to crash from unbound halfedges -> investigate

	public static void splitBrushByBrush(CSGBrush a, CSGBrush b, out Dictionary<GameObject, List<Surface>> interiors, out Dictionary<GameObject, List<Surface>> exteriors) {

		/*
		GameObject s = new GameObject();
		s.name = "SPLIT";
		//*/

		interiors = new Dictionary<GameObject, List<Surface>>();
		exteriors = new Dictionary<GameObject, List<Surface>>();

		foreach (GameObject ga in a.surfaces.Keys) {

			interiors[ga] = new List<Surface>();
			exteriors[ga] = new List<Surface>();

			List<Surface> subsurfaces = a.surfaces[ga];

			foreach(GameObject gb in b.surfaces.Keys) {

				List<Surface> interior;
				List<Surface> exterior;

				Vector3 relativeTranslation = RelativeTranslation(ga.transform, gb.transform);
				Quaternion relativeRotation = RelativeRotation(ga.transform, gb.transform);
				Vector3 relativeScale = RelativeScale(ga.transform, gb.transform);

				splitSurfacesBySurfaces(subsurfaces, b.surfaces[gb],
					relativeTranslation, relativeRotation, relativeScale,
					out interior, out exterior);

				/*
				GameObject ma = GameObject.CreatePrimitive(PrimitiveType.Sphere);
				ma.GetComponent<MeshFilter>().mesh.Clear();
				ma.GetComponent<MeshFilter>().mesh = GenerateMesh(interior, Resources.Load<Material>("Materials/Blue"));
				ma.transform.position = ga.transform.position;
				ma.transform.rotation = ga.transform.rotation;
				ma.transform.localScale = ga.transform.localScale;
				ma.name = "interior";

				GameObject mb = GameObject.CreatePrimitive(PrimitiveType.Sphere);
				mb.GetComponent<MeshFilter>().mesh.Clear();
				mb.GetComponent<MeshFilter>().mesh = GenerateMesh(exterior, Resources.Load<Material>("Materials/Blue"));
				mb.transform.position = ga.transform.position;
				mb.transform.rotation = ga.transform.rotation;
				mb.transform.localScale = ga.transform.localScale;
				mb.name = "exterior";
				//*/

				interiors[ga].AddRange(interior);
				subsurfaces = exterior;
			}
			exteriors[ga] = subsurfaces;
		}
	}

	//

	/*
	public static Mesh GenerateInteriorMesh(List<Surface> surfaces, Material material) {

		List<Vector3> vertices = new List<Vector3>();
		List<Vector3> normals = new List<Vector3>();
		//List<Vector2> uvs = new List<Vector2>();
		List<int> triangles = new List<int>();

		foreach (Surface s in surfaces) {
			foreach (Polygon p in s.polys) {
				if (p.material == material) {
					Vector3 origin = p.hedges[0].start;
					vertices.Add(origin);
					normals.Add(p.plane.normal * -1);
					for (int i = 1; i < p.hedges.Count; i++) {
						HalfEdge h = p.hedges[i];
						vertices.Add(h.start);
						normals.Add(p.plane.normal * -1);
						vertices.Add(h.next.start);
						normals.Add(p.plane.normal * -1);
						triangles.Add(vertices.LastIndexOf(origin));
						triangles.Add(vertices.LastIndexOf(h.start));
						triangles.Add(vertices.LastIndexOf(h.next.start));
					}
				}
			}
		}

		Mesh mesh = new Mesh();
		mesh.vertices = vertices.ToArray();
		mesh.normals = normals.ToArray();
		mesh.triangles = triangles.ToArray();

		return mesh;
	}
	//*/

	public static Mesh GenerateMesh(List<Surface> surfaces, Material material) {

		List<Vector3> vertices = new List<Vector3>();
		List<Vector3> normals = new List<Vector3>();
		//List<Vector2> uvs = new List<Vector2>();
		List<int> triangles = new List<int>();

		foreach (Surface s in surfaces) {
			foreach (Polygon p in s.polys) {
				if (p.material == material) {
					Vector3 origin = p.hedges[p.hedges.Count - 1].next.start;
					vertices.Add(origin);
					normals.Add(p.plane.normal);
					for (int i = p.hedges.Count - 2; i > 0; i--) {
						HalfEdge h = p.hedges[i];
						vertices.Add(h.next.start);
						normals.Add(p.plane.normal);
						vertices.Add(h.start);
						normals.Add(p.plane.normal);
						triangles.Add(vertices.LastIndexOf(origin));
						triangles.Add(vertices.LastIndexOf(h.next.start)); //Technically always second to last index
						triangles.Add(vertices.LastIndexOf(h.start)); //Technically always last index
					}
				}
			}
		}

		Mesh mesh = new Mesh();
		mesh.vertices = vertices.ToArray();
		mesh.normals = normals.ToArray();
		mesh.triangles = triangles.ToArray();

		return mesh;
	}

	//

	public static List<Polygon> DuplicateInstances(List<Polygon> polygons) {

		List<Polygon> duplicates = new List<Polygon>();
		foreach (Polygon p in polygons) {
			List<HalfEdge> halfedges = new List<HalfEdge>();
			Polygon d_polygon = new Polygon(halfedges, p.plane, p.material);
			foreach(HalfEdge h in p.hedges) {
				HalfEdge d_halfedge = new HalfEdge(h.start, d_polygon);
				halfedges.Add(d_halfedge);
			}
			d_polygon.hedges = halfedges;
			duplicates.Add(d_polygon);
		}

		//NOTE: HE still present here
		/*
		foreach(Polygon d in duplicates) {
			GameObject g = new GameObject();
			g.name = "Polygon";
			foreach (HalfEdge h in d.hedges) {
				GameObject he = GameObject.CreatePrimitive(PrimitiveType.Cube);
				he.name = "h";
				he.transform.position = h.start;
				he.transform.localScale *= 0.05f;
				he.GetComponent<MeshRenderer>().material.SetColor("Color", Color.green);
				he.transform.SetParent(g.transform);
			}
		}
		//*/

		CSGBrushFactory.BindSerialHalfEdges(duplicates);

		//NOTE: HE still present here
		/*
		foreach(Polygon d in duplicates) {
			GameObject g = new GameObject();
			g.name = "Poly";
			foreach (HalfEdge h in d.hedges) {
				GameObject ep = new GameObject();
				ep.name = "Edge Pair";
				ep.transform.position = h.start + h.next.start;
				ep.transform.SetParent(g.transform);

				GameObject he = GameObject.CreatePrimitive(PrimitiveType.Cube);
				he.name = "h";
				he.transform.position = h.start;
				he.transform.localScale *= 0.05f;
				he.GetComponent<MeshRenderer>().material.SetColor("Color", Color.magenta);
				he.transform.SetParent(ep.transform);

				GameObject te = GameObject.CreatePrimitive(PrimitiveType.Sphere);
				te.name = "t";
				te.transform.position = h.next.start;
				te.transform.localScale *= 0.0675f;
				te.GetComponent<MeshRenderer>().material.SetColor("Color", Color.green);
				te.transform.SetParent(ep.transform);
			}
		}
		//*/

		CSGBrushFactory.BindAntiparallelHalfEdges(duplicates);

		//NOTE: HE still present here
		/*
		foreach(Polygon d in duplicates) {
			GameObject g = new GameObject();
			g.name = "Poly";
			foreach (HalfEdge h in d.hedges) {
				GameObject ep = new GameObject();
				ep.name = "Edge Pair";
				ep.transform.position = h.start + h.next.start;
				ep.transform.SetParent(g.transform);

				GameObject he = GameObject.CreatePrimitive(PrimitiveType.Cube);
				he.name = "h";
				he.transform.position = h.start;
				he.transform.localScale *= 0.05f;
				he.GetComponent<MeshRenderer>().material.SetColor("Color", Color.magenta);
				he.transform.SetParent(ep.transform);

				GameObject te = GameObject.CreatePrimitive(PrimitiveType.Sphere);
				te.name = "t";
				te.transform.position = h.next.start;
				te.transform.localScale *= 0.0675f;
				te.GetComponent<MeshRenderer>().material.SetColor("Color", Color.green);
				te.transform.SetParent(ep.transform);
			}
		}
		//*/

		//ERROR NOT IN DUPLICATEINSTANCES

		return duplicates;
	}

	public static List<Polygon> CreateDuplicateInstances(List<Polygon> source) {
		// Consider revising -> efficiency?

		//Create new Polygon and HalfEdge instances
		List<Polygon> pds = new List<Polygon>();
		foreach (Polygon p in source) {
			List<HalfEdge> hds = new List<HalfEdge>();
			Polygon pd = new Polygon(hds, p.plane, p.material);
			foreach (HalfEdge h in p.hedges) {
				HalfEdge hd = new HalfEdge(h.start, pd);
				hds.Add(hd);
			}
			pd.hedges = hds;
			//if (pd.hedges.Count > 0) { //Handle empty polygons
				pds.Add(pd);
			//}
		}
		CSGBrushFactory.BindSerialHalfEdges(pds);
		CSGBrushFactory.BindAntiparallelHalfEdges(pds);
		return pds;
	}

	// WIP -> Reference RenderExteriorSurfaces once functional
	public static Mesh RenderInteriorSurfaces(List<Surface> surfs) {

		List<Vector3> vertices = new List<Vector3>();
		List<Vector2> uvmaps = new List<Vector2>();
		List<int> triangles = new List<int>();
		List<Vector3> normals = new List<Vector3>();

		//Fix UVs
		//Fix normals

		foreach (Surface s in surfs) {
			foreach (Polygon p in s.polys) {

				// TO DO - CHANGE FUNCTIONALITY TO PRODUCE DICTIONARY OF MATERIALS TO MESHES/GAMEOBJECTS
				if (p.material != null) {
					Vector3 origin = p.hedges[0].start;

					/*
					if (!vertices.Contains(origin)) {
						vertices.Add(origin);
						normals.Add(p.plane.normal);
					}
					for (int i = 1; i < p.hedges.Count; i++) {
						HalfEdge h = p.hedges[i];
						if (!vertices.Contains(h.start)) {
							vertices.Add(h.start);
							normals.Add(p.plane.normal);
						}
						if (!vertices.Contains(h.next.start)) {
							vertices.Add(h.next.start);
							normals.Add(p.plane.normal);
						}
						triangles.Add(vertices.IndexOf(origin));
						triangles.Add(vertices.IndexOf(h.start));
						triangles.Add(vertices.IndexOf(h.next.start));
					}
					//*/

					//Add duplicate vertices	
					vertices.Add(origin);
					normals.Add(p.plane.normal * -1);
					for (int i = 1; i < p.hedges.Count; i++) {
						HalfEdge h = p.hedges[i];
						vertices.Add(h.start);
						normals.Add(p.plane.normal * -1);
						vertices.Add(h.next.start);
						normals.Add(p.plane.normal * -1);
						triangles.Add(vertices.LastIndexOf(origin));
						triangles.Add(vertices.LastIndexOf(h.start));
						triangles.Add(vertices.LastIndexOf(h.next.start));
					}
				}
			}
		}

		foreach (Vector3 v in vertices) {
			uvmaps.Add(new Vector2(0, 0));
		}

		Vector3[] verts = vertices.ToArray();
		Vector2[] uvs = uvmaps.ToArray();
		int[] tris = triangles.ToArray();
		Vector3[] norms = normals.ToArray();

		Mesh mesh = new Mesh();
		mesh.vertices = verts;
		mesh.uv = uvs;
		mesh.triangles = tris;
		mesh.normals = norms;
		//mesh.RecalculateNormals();

		return mesh;
	}

	public static Mesh RenderExteriorSurfaces(List<Surface> surfs) {

		List<Vector3> vertices = new List<Vector3>();
		List<Vector2> uvmaps = new List<Vector2>();
		List<int> triangles = new List<int>();
		List<Vector3> normals = new List<Vector3>();

		//Fix Normals/UVs

		foreach (Surface s in surfs) {
			foreach (Polygon p in s.polys) {
				if (p.material != null) {
					Vector3 origin = p.hedges[p.hedges.Count - 1].next.start;

					//Exclude duplicate vertices (smoothing, but incorrect normals)
					/*
					if (!vertices.Contains(origin)) {
						vertices.Add(origin);
						normals.Add(p.plane.normal);
					}
					for (int i = p.hedges.Count - 2; i > 0; i--) {
						HalfEdge h = p.hedges[i];
						if (!vertices.Contains(h.next.start)) {
							vertices.Add(h.next.start);
							normals.Add(p.plane.normal);
						}
						if (!vertices.Contains(h.start)) {
							vertices.Add(h.start);
							normals.Add(p.plane.normal);
						}
						triangles.Add(vertices.IndexOf(origin));
						triangles.Add(vertices.IndexOf(h.next.start));
						triangles.Add(vertices.IndexOf(h.start));
					}
					//*/

					//Add Duplicate Vertices (No smoothing, but simplified normals)
					vertices.Add(origin);
					normals.Add(p.plane.normal);
					for (int i = p.hedges.Count - 2; i > 0; i--) {
						HalfEdge h = p.hedges[i];
						vertices.Add(h.next.start);
						normals.Add(p.plane.normal);
						vertices.Add(h.start);
						normals.Add(p.plane.normal);
						triangles.Add(vertices.LastIndexOf(origin));
						triangles.Add(vertices.LastIndexOf(h.next.start)); //Technically always second to last index
						triangles.Add(vertices.LastIndexOf(h.start)); //Technically always last index
					}
				}
			}
		}

		foreach (Vector3 v in vertices) {
			uvmaps.Add(new Vector2(0, 0));
		}

		Vector3[] verts = vertices.ToArray();
		Vector2[] uvs = uvmaps.ToArray();
		int[] tris = triangles.ToArray();
		Vector3[] norms = normals.ToArray();

		Mesh mesh = new Mesh();
		mesh.vertices = verts;
		mesh.uv = uvs;
		mesh.triangles = tris;
		mesh.normals = norms; //CalculateNormals(verts, tris);
							  //mesh.RecalculateNormals();

		return mesh;
	}

	/* FLAWED - VERTICES BASED ON DIFFERENT POINTS OF REFERENCE
	//Temporary 
	// -> Revise to take vertex and triangle lists?
	// -> Create supporting methods
	public static Mesh RenderCombinedMesh(Mesh a, Mesh b) {

		Mesh mesh = new Mesh();

		List<Vector3> temp = new List<Vector3>();
		temp.AddRange(a.vertices);
		temp.AddRange(b.vertices);
		mesh.vertices = temp.ToArray();

		//Take existing mesh as parameter and add 1?
		//Take dynamic number of parameters and set equal to total?

		int[] btris = new int[b.triangles.Length];
		for (int i = 0; i < b.triangles.Length; i++) {
			btris[i] = b.triangles[i] + a.vertices.Length;
		}

		mesh.subMeshCount = 2;
		mesh.SetTriangles(a.triangles, 0);
		mesh.SetTriangles(btris, 1);

		return mesh;
	}
	//*/

	//WIP - Best bet so far
	public static Vector3[] CalculateNormals(Vector3[] verts, int[] tris) {
		//*
		Vector3[] norms = new Vector3[verts.Length];

		foreach (Vector3 v in verts) {
			Vector3 n = v.normalized; //Vector3.zero;
			int index = Array.IndexOf(verts, v);
			int[] faces = Enumerable.Range(0, tris.Length).Where(i => tris[i] == index).ToArray();
			for (int i = 0; i < faces.Length; i++) {
				faces[i] -= faces[i] % 3;
				Vector3 facet = Vector3.Cross(
					verts[tris[faces[i]]] - verts[tris[faces[i] + 1]],
					verts[tris[faces[i]]] - verts[tris[faces[i] + 2]]);
				float area = Vector3.Dot(
					verts[tris[faces[i]]] - verts[tris[faces[i] + 1]],
					verts[tris[faces[i]]] - verts[tris[faces[i] + 2]]);
				float angle = Mathf.Acos(Vector3.Dot(n, facet) / (Vector3.Magnitude(n) * Vector3.Magnitude(facet)));
				n += facet * area * angle;
			}
			norms[index] = n.normalized;
		}

		return norms;
		//*/
	}

	/*
	public static Vector3[] CalculateWeightedNormals(List<Surface> surfs) {

		foreach(Surface s in surfs) {

			foreach(Polygon a in s.polys) {
				Vector3 n = a.plane.normal;
				foreach(HalfEdge h in a.hedges) {
					foreach(Polygon b in s.polys) {
						if (a == b) { break; }

						if(b.hedges.Contains())
					}
				}

			}

		}
	}
	//*/

	public static Vector2[] RecalculateUVs(Vector3[] verts, GameObject refObj) {

		Mesh refMesh = refObj.GetComponent<MeshFilter>().mesh;

		Vector2[] uvs = new Vector2[verts.Length];

		for (int i = 0; i < verts.Length; i++) {
			/*
			int index = Array.IndexOf(refMesh.vertices, verts[i]);
			if(index >= 0) {
				uvs[i] = refMesh.uv[index];
			}
			//*/

			int r1 = 0;
			int r2 = 1;
			float d1 = Vector3.Distance(verts[i], refMesh.vertices[r1]);
			float d2 = Vector3.Distance(verts[i], refMesh.vertices[r1]);
			for (int j = 2; j < refMesh.vertices.Length; j++) {
				float dj = Vector3.Distance(verts[i], refMesh.vertices[j]);
				if (dj < Vector3.Distance(verts[i], refMesh.vertices[r1])) {
					r1 = j;
					d1 = dj;
				} else if (dj < Vector3.Distance(verts[i], refMesh.vertices[r2])) {
					r2 = j;
					d2 = dj;
				}
			}

			uvs[i] = new Vector2(
				refMesh.uv[r1].x * (d1 / (d1 + d2)) + refMesh.uv[r2].x * (d2 / (d1 + d2)),
				refMesh.uv[r1].y * (d1 / (d1 + d2)) + refMesh.uv[r2].y * (d2 / (d1 + d2)));
			Debug.Log(uvs[i]);
		}

		return uvs;
	}

	// Split into two methods
	/*
	public static Mesh RenderSurfaces(List<Surface> surfs) {

		Vector3[] verts;
		Vector2[] uvs;
		int[] tris;

		List<Vector3> vertices = new List<Vector3>();
		List<Vector2> uvmaps = new List<Vector2>();
		List<int> triangles = new List<int>();

		//Fix UVs

		foreach (Surface s in surfs) {
			foreach(Polygon p in s.polys) {
				/*
				Vector3 origin = p.hedges[0].start.position;
				if (!vertices.Contains(origin)) {
					vertices.Add(origin);
					uvmaps.Add(new Vector2(0.5f, 1));
				}
				for (int i = 1; i < p.hedges.Count; i++) {
					HalfEdge h = p.hedges[i];
					if (!vertices.Contains(h.start.position)) {
						vertices.Add(h.start.position);
						uvmaps.Add(new Vector2(0.5f, 1));
					}
					if (!vertices.Contains(h.end.position)) {
						vertices.Add(h.end.position);
						uvmaps.Add(new Vector2(0.5f, 1));
					}
					triangles.Add(vertices.IndexOf(origin));
					triangles.Add(vertices.IndexOf(h.start.position));
					triangles.Add(vertices.IndexOf(h.end.position));
				}
				//

				// Invert normals by looping in reverse order? -> Seems to Work
				//* 
				Vector3 origin = p.hedges[p.hedges.Count - 1].end.position;
				if (!vertices.Contains(origin)) {
					vertices.Add(origin);
				}
				for (int i = p.hedges.Count - 2; i > 0; i--) {
					HalfEdge h = p.hedges[i];
					if (!vertices.Contains(h.end.position)) {
						vertices.Add(h.end.position);
					}
					if (!vertices.Contains(h.start.position)) {
						vertices.Add(h.start.position);
					}
					triangles.Add(vertices.IndexOf(origin));
					triangles.Add(vertices.IndexOf(h.end.position));
					triangles.Add(vertices.IndexOf(h.start.position));
				}
				//
			}
		}

		foreach(Vector3 v in vertices) {
			uvmaps.Add(new Vector2(v.x, v.z));
		}

		verts = vertices.ToArray();
		uvs = uvmaps.ToArray();
		tris = triangles.ToArray();

		Mesh mesh = new Mesh();
		mesh.vertices = verts;
		mesh.uv = uvs;
		mesh.triangles = tris;

		return mesh;
	}
	//*/

	//Temporary - Replace with CSGBrushes, CSGOperations
	//INCOMPLETE
	public static void RenderModel(List<Surface> ain, List<Surface> aout, List<Surface> bin, List<Surface> bout) {

		// Placeholder Field
		int operation = 1;
		// -1 - Subtract, 0 - Intersect, 1 - Add

		List<Surface> surfs = new List<Surface>();

		// NOTE: Need to invert interior normals
		if (operation == -1) {
			surfs.AddRange(aout);
			surfs.AddRange(bin);
		} else if (operation == 0) {
			surfs.AddRange(ain);
			surfs.AddRange(bin);
		} else if (operation == 1) {
			surfs.AddRange(aout);
			surfs.AddRange(bout);
		}

		Vector3[] verts;
		Vector2[] uvs;
		int[] tris;


	}

	// New class : Surface (contains polygon, material, and uvs?)
	// A Brush is a list of surfaces?

	//Alternatively
	// Surface is a list of polygons (describing a convex polytope), and a Category (inside or outside) 
	//Classified on split -> Outside overwrites inside?
	//List/Dictionary relative to each cutting volume?
	// Brush is a list of surfaces, a material, a mesh, etc.

	// public static split(Polygon poly, Plane plane, out List<Polygon> polys, out Category ...) -> Category (if list len 1 = aligned OR invaligned, if list len 2 = outside meaning {out, in} OR inside meaning {in, out}

	// Consider updates ... Delegate event bound to brush?

	// NOTE:
	// Split Polytopes are missing internal faces, causes error on subsequent cuts (unbound/nonexistent twins.. ect.)
	// Technically should be exclude (filled by corresponding cuts from second volume) ... restructure to handles volumes that are not completely enclosed?

	// Pass in transform to rotate planes relative to brush?
}
