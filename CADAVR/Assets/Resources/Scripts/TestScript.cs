﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

public class TestScript : MonoBehaviour {

	// Use this for initialization
	void Start () {

        //Confirms (plane.normal * -plane.distance) is on plane
        /*
        GameObject plane = GameObject.CreatePrimitive(PrimitiveType.Plane);
        plane.transform.position = new Vector3(-3, -5, 2);
        plane.transform.up = new Vector3(1, 1, 1) * -1;

        Plane pl = new Plane(plane.transform.up, plane.transform.position);

        GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        sphere.transform.localScale *= 0.1f;
        sphere.transform.position = pl.normal * -pl.distance;
        //*/

        //Demonstrates functionality of ThreePlaneIntersection method
        /*
        GameObject planeA = GameObject.CreatePrimitive(PrimitiveType.Plane);
        planeA.transform.position = new Vector3(-3, -5, 2);
        planeA.transform.up = new Vector3(1, 1, 1);

        GameObject planeB = GameObject.CreatePrimitive(PrimitiveType.Plane);
        planeB.transform.position = new Vector3(-2, 1, 1);
        planeB.transform.up = new Vector3(1, 2, -1);

        GameObject planeC = GameObject.CreatePrimitive(PrimitiveType.Plane);
        planeC.transform.position = new Vector3(1, 3, -2);
        planeC.transform.up = new Vector3(5, 1, 0.5f);

        Plane a = new Plane(planeA.transform.up, planeA.transform.position);
        Plane b = new Plane(planeB.transform.up, planeB.transform.position);
        Plane c = new Plane(planeC.transform.up, planeC.transform.position);

        float d;
        CSGBrushFactory.CheckPlaneIntersection(a, b, c, out d);

        GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        sphere.transform.localScale *= 0.1f;
        sphere.transform.position = CSGBrushFactory.FindPlaneIntersection(a, b, c, d);

        Debug.Log(a.GetSide(sphere.transform.position));
        Debug.Log(a.GetDistanceToPoint(sphere.transform.position));
        //*/

        //Checks functionality of CompileVertices method
        /*
        Plane[] plns = new Plane[7];

        //Cube
        plns[0] = new Plane(Vector3.up, Vector3.up);
        plns[1] = new Plane(-Vector3.up, -Vector3.up);
        plns[2] = new Plane(Vector3.right, Vector3.right);
        plns[3] = new Plane(-Vector3.right, -Vector3.right);
        plns[4] = new Plane(Vector3.forward, Vector3.forward);
        plns[5] = new Plane(-Vector3.forward, -Vector3.forward);
        plns[6] = new Plane(Vector3.one, new Vector3(0.4f, 0.4f, 0.4f));

        Vertex[] vrts = CSGBrushFactory.CompileVertices(plns);

        foreach(Vertex v in vrts) {
            GameObject g = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            g.transform.position = v.position;
            g.transform.localScale *= 0.1f;
        }
        
        foreach(Plane p in vrts[5].planes) {
            GameObject g = GameObject.CreatePrimitive(PrimitiveType.Plane);
            g.transform.position = p.normal * -p.distance;
            g.transform.up = p.normal;
            g.transform.localScale *= 0.1f;
        }
        //*/

        //Checks if CompileVertices can find vertices of an existing mesh
        //NOTE: Does not work for spheres/capsules (presumably too complex; use custom, simple models for brushes)
        /*
        GameObject cylinder = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
                              //Instantiate(Resources.Load<GameObject>("Models/IcoSphere")); 
        cylinder.GetComponent<MeshRenderer>().material = Resources.Load<Material>("Materials/Red");

        List<Plane> planes = new List<Plane>();
        
        //*
        Mesh mesh = cylinder.GetComponent<MeshFilter>().mesh;
        for (int i = 0; i < mesh.triangles.Length; i+= 3) {
            planes.Add(new Plane(
                mesh.vertices[mesh.triangles[i]],
                mesh.vertices[mesh.triangles[i + 1]], 
                mesh.vertices[mesh.triangles[i + 2]]));
        }
        //*/

        /*
        planes.Add(new Plane(Vector3.one, Vector3.one * 0.2f));
        planes.Add(new Plane(-Vector3.one, -Vector3.one * 0.1f));
        //*/

        /*
        planes.Add(new Plane(Vector3.up, Vector3.up));
        planes.Add(new Plane(-Vector3.up, -Vector3.up));
        planes.Add(new Plane(Vector3.right, Vector3.right));
        planes.Add(new Plane(-Vector3.right, -Vector3.right));
        planes.Add(new Plane(Vector3.forward, Vector3.forward));
        planes.Add(new Plane(-Vector3.forward, -Vector3.forward));      
        planes.Add(new Plane(Vector3.one, Vector3.one * 0.4f));
        planes.Add(new Plane(new Vector3(1, 1, -1), new Vector3(1, 1, -1) * 0.4f));
        //*/

		/*
        //Check that planes are unique -> Incorporate into function?
        for (int i = 0; i < planes.Count; i++) {
            for (int j = i + 1; j < planes.Count; j++) {
                if(Vector3.Cross(planes[i].normal, planes[j].normal) == Vector3.zero &&
                   Vector3.Dot(planes[i].normal, planes[j].normal) == 1) {
                    planes.Remove(planes[j]);
                    //NOTE: Does not cull parallel planes, only duplicates (not optimized) -> Probably not worth implementing
                }
            }
        }

        Debug.Log(planes.Count); //Includes extra for cylinder, cause uncertain

		Dictionary<Vector3, List<Plane>> verts = CSGBrushFactory.CompileVertices(planes);
		Dictionary<Plane, List<HalfEdge>> edges = CSGBrushFactory.CompileHalfEdges(verts);
		List<Polygon> polys = CSGBrushFactory.CompilePolygons(edges, cylinder.GetComponent<MeshRenderer>().material);

		//Temporary material assignment for new render testing
		foreach(Polygon p in polys) {
			p.material = cylinder.GetComponent<MeshRenderer>().material;
		}
		//*/

		/*
		foreach (Polygon p in polys) {
            GameObject poly = new GameObject();
            poly.name = "Polygon";
            for (int i = 0; i < p.hedges.Count; i++) {
                GameObject g = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                g.transform.position = p.hedges[i].start;
                g.transform.localScale *= 0.05f;
                g.transform.SetParent(poly.transform);
            }

            //Exploded View Modifier
            Vector3 explosionVector = p.plane.normal * 0.0f;
            poly.transform.position += explosionVector;

            //Vertex Connection Viewer
            GameObject b = GameObject.CreatePrimitive(PrimitiveType.Cube);
            b.name = "norm";
            b.transform.localScale = Vector3.one * 0.05f;
            b.transform.position = (p.plane.normal * -p.plane.distance);
            b.GetComponent<MeshRenderer>().material = Resources.Load<Material>("Materials/Blue");
            //b.GetComponent<MeshRenderer>().enabled = false;

            Vector3[] vpos = new Vector3[p.hedges.Count + 1];
            for (int k = 0; k < p.hedges.Count; k++) {
                vpos[k] = p.hedges[k].start + explosionVector;
            }
            vpos[vpos.Length - 1] = vpos[0];

            b.AddComponent<LineRenderer>();
            b.GetComponent<LineRenderer>().SetWidth(0.03f, 0.03f);
            b.GetComponent<LineRenderer>().material = Resources.Load<Material>("Materials/Green");
            b.GetComponent<LineRenderer>().SetVertexCount(vpos.Length);
            b.GetComponent<LineRenderer>().SetPositions(vpos);

			// Half Edge Revised Test

			if (p == polys[3]) {
				foreach (HalfEdge h in p.hedges) {
					GameObject ep = new GameObject();
					ep.name = "Edge Pair";
					ep.transform.position = h.start + h.next.start;

					GameObject he = GameObject.CreatePrimitive(PrimitiveType.Cube);
					he.name = "h";
					he.transform.position = h.start;
					he.transform.localScale *= 0.05f;
					he.GetComponent<MeshRenderer>().material = Resources.Load<Material>("Materials/Blue");
					he.transform.SetParent(ep.transform);

					GameObject te = GameObject.CreatePrimitive(PrimitiveType.Sphere);
					te.name = "t";
					te.transform.position = h.twin.start;
					te.transform.localScale *= 0.0675f;
					te.GetComponent<MeshRenderer>().material = Resources.Load<Material>("Materials/Green");
					te.transform.SetParent(ep.transform);
				}
			}

		}
		//*/

		// Polygon Split Test
		/* 
		Plane testplane = new Plane(new Vector3(0, 1, 0), Vector3.up);
		GameObject tp = GameObject.CreatePrimitive(PrimitiveType.Plane);
		tp.transform.position = (testplane.normal * -testplane.distance);
		tp.transform.up = testplane.normal;

		//Debug.Log("Test 1: " + testplane.SameSide(new Vector3(1, 0, 0), Vector3.zero));
		//Debug.Log("Test 2: " + testplane.SameSide(new Vector3(-1, 0, 0), Vector3.zero));

		Polygon inside;
		Polygon outside;
		CSGBrushComparator.splitPolygonByPlane(plys[3], testplane, out inside, out outside);

		Polygon[] testpolys = new Polygon[2] { inside, outside };

		Debug.Log(testpolys[0] + "    " + testpolys[1]);

		foreach(Polygon p in testpolys) {
			GameObject poly = new GameObject();
			poly.name = "Polygon";

			GameObject b = GameObject.CreatePrimitive(PrimitiveType.Cube);
			b.name = "norm";
			b.transform.localScale = Vector3.one * 0.06f;
			b.transform.position = (p.plane.normal * -p.plane.distance);
			b.GetComponent<MeshRenderer>().material = Resources.Load<Material>("Materials/Green");

			foreach (HalfEdge h in p.hedges) {
				GameObject ep = new GameObject();
				ep.name = "Edge Pair";
				ep.transform.position = h.start.position + h.end.position;
				ep.transform.SetParent(poly.transform);

				GameObject he = GameObject.CreatePrimitive(PrimitiveType.Cube);
				he.name = "h";
				he.transform.position = h.start.position;
				he.transform.localScale *= 0.05f;
				he.GetComponent<MeshRenderer>().material = Resources.Load<Material>("Materials/Blue");
				he.transform.SetParent(ep.transform);

				GameObject te = GameObject.CreatePrimitive(PrimitiveType.Sphere);
				te.name = "t";
				te.transform.position = h.twin.start.position;
				te.transform.localScale *= 0.0675f;
				te.GetComponent<MeshRenderer>().material = Resources.Load<Material>("Materials/Green");
				te.transform.SetParent(ep.transform);
			}
		}
		//*/

		// Polytope Split Test
		/*
		Plane testplane = new Plane(new Vector3(1, 1, 1), Vector3.zero);
		GameObject tp = GameObject.CreatePrimitive(PrimitiveType.Plane);
		tp.transform.position = (testplane.normal * -testplane.distance);
		tp.transform.up = testplane.normal;

		List<Polygon> testtope = polys;
		Surface testsurf = new Surface(testtope);

		List<Polygon> interior = new List<Polygon>();
		List<Polygon> exterior = new List<Polygon>();
		CSGBrushComparator.splitSurfaceByPlane(testsurf, testplane, out interior, out exterior);

		GameObject gin = new GameObject();
		gin.name = "Interior";

		foreach (Polygon p in interior) {
			GameObject poly = new GameObject();
			poly.name = "Polygon";
			poly.transform.SetParent(gin.transform);

			foreach (HalfEdge h in p.hedges) {
				GameObject ep = new GameObject();
				ep.name = "Edge Pair";
				ep.transform.position = h.start + h.next.start;
				ep.transform.SetParent(poly.transform);

				GameObject he = GameObject.CreatePrimitive(PrimitiveType.Cube);
				he.name = "h";
				he.transform.position = h.start;
				he.transform.localScale *= 0.05f;
				he.GetComponent<MeshRenderer>().material = Resources.Load<Material>("Materials/Blue");
				he.transform.SetParent(ep.transform);

				GameObject te = GameObject.CreatePrimitive(PrimitiveType.Sphere);
				te.name = "t";
				te.transform.position = h.twin.start;
				te.transform.localScale *= 0.0675f;
				te.GetComponent<MeshRenderer>().material = Resources.Load<Material>("Materials/Green");
				te.transform.SetParent(ep.transform);
			}
		}

		GameObject gout = new GameObject();
		gout.name = "Exterior";

		foreach (Polygon p in exterior) {
			GameObject poly = new GameObject();
			poly.name = "Polygon";
			poly.transform.SetParent(gout.transform);

			foreach (HalfEdge h in p.hedges) {
				GameObject ep = new GameObject();
				ep.name = "Edge Pair";
				ep.transform.position = h.start + h.next.start;
				ep.transform.SetParent(poly.transform);

				GameObject he = GameObject.CreatePrimitive(PrimitiveType.Cube);
				he.name = "h";
				he.transform.position = h.start;
				he.transform.localScale *= 0.05f;
				he.GetComponent<MeshRenderer>().material = Resources.Load<Material>("Materials/Blue");
				he.transform.SetParent(ep.transform);

				GameObject te = GameObject.CreatePrimitive(PrimitiveType.Sphere);
				te.name = "t";
				te.transform.position = h.twin.start;
				te.transform.localScale *= 0.0675f;
				te.GetComponent<MeshRenderer>().material = Resources.Load<Material>("Materials/Green");
				te.transform.SetParent(ep.transform);
			}
		}
		//*/

		// "Brush" Split Test
		/*
		GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);

		List<Plane> cubeplanes = new List<Plane>();

		Mesh cmesh = cube.GetComponent<MeshFilter>().mesh;
		for (int i = 0; i < cmesh.triangles.Length; i += 3) {
			cubeplanes.Add(new Plane(
				cmesh.vertices[cmesh.triangles[i]],
				cmesh.vertices[cmesh.triangles[i + 1]],
				cmesh.vertices[cmesh.triangles[i + 2]]));
		}
		//*/

		/*
		cube.transform.forward = new Vector3(1, 1, 0);
		Quaternion rotationDelta = Quaternion.Inverse(cylinder.transform.rotation) * cube.transform.rotation;

		for (int i = 0; i < cubeplanes.Count; i++) {
			Plane np = cubeplanes[i];
			np.normal = rotationDelta * cubeplanes[i].normal;
			cubeplanes[i] = np;
		}

		float scaleFactor = 1.5f;
		cube.transform.localScale = Vector3.one * scaleFactor;
		for (int i = 0; i < cubeplanes.Count; i++) {
			Plane np = cubeplanes[i];
			np.distance = cubeplanes[i].distance * scaleFactor;
			cubeplanes[i] = np;
		}
		//*/

		/*
		float offset = 0.4f;
		//cubeplanes.Add(new Plane(Vector3.up, Vector3.up * offset));
		//cubeplanes.Add(new Plane(-Vector3.up, -Vector3.up * offset));
		//cubeplanes.Add(new Plane(Vector3.right, Vector3.right * offset));
		//cubeplanes.Add(new Plane(-Vector3.right, -Vector3.right * offset));
		//cubeplanes.Add(new Plane(Vector3.forward, Vector3.forward * offset));
		//cubeplanes.Add(new Plane(-Vector3.forward, -Vector3.forward * offset));

		//cubeplanes.Add(new Plane(new Vector3(0, 1, 0).normalized, new Vector3(0, 1, 0).normalized * offset));
		//cubeplanes.Add(new Plane(new Vector3(0, 1, 0).normalized * -1, new Vector3(0, 1, 0).normalized * -offset));
		//cubeplanes.Add(new Plane(new Vector3(1, 0, 1).normalized, new Vector3(1, 0, 1).normalized * offset));
		//cubeplanes.Add(new Plane(new Vector3(1, 0, 1).normalized * -1, new Vector3(1, 0, 1).normalized * -offset));
		//cubeplanes.Add(new Plane(new Vector3(1, 0, -1).normalized, new Vector3(1, 0, -1).normalized * offset));
		//cubeplanes.Add(new Plane(new Vector3(1, 0, -1).normalized * -1, new Vector3(1, 0, -1).normalized * -offset));

		cubeplanes.Add(new Plane(new Vector3(1, 1, 0).normalized, new Vector3(1, 1, 0).normalized * offset));
		cubeplanes.Add(new Plane(new Vector3(1, 1, 0).normalized * -1, new Vector3(1, 1, 0).normalized * -offset));
		cubeplanes.Add(new Plane(new Vector3(-1, 1, 0).normalized, new Vector3(-1, 1, 0).normalized * offset));
		cubeplanes.Add(new Plane(new Vector3(-1, 1, 0).normalized * -1, new Vector3(-1, 1, 0).normalized * -offset));
		cubeplanes.Add(new Plane(new Vector3(0, 0, 1).normalized, new Vector3(0, 0, 1).normalized * offset));
		cubeplanes.Add(new Plane(new Vector3(0, 0, 1).normalized * -1, new Vector3(0, 0, 1).normalized * -offset));
		//*/

		/*
		GameObject c = new GameObject();
		c.name = "Cube";

		foreach (Plane p in cubeplanes){
			GameObject g = new GameObject();
			g.name = "Face";
			g.transform.SetParent(c.transform);

			GameObject f = GameObject.CreatePrimitive(PrimitiveType.Plane);
			f.name = "Front";
			f.transform.position = (p.distance * -p.normal);
			f.transform.up = p.normal;
			f.transform.localScale = Vector3.one * offset / 5;
			f.transform.SetParent(g.transform);

			GameObject b = GameObject.CreatePrimitive(PrimitiveType.Plane);
			b.name = "Back";
			b.transform.position = (p.distance * -p.normal);
			b.transform.up = -p.normal;
			b.transform.localScale = Vector3.one * offset / 5;
			b.transform.SetParent(g.transform);
		}
		//*/

		/*
		Dictionary<Vector3, List<Plane>> cubeverts = CSGBrushFactory.CompileVertices(cubeplanes);
		Dictionary<Plane, List<HalfEdge>> cubeedges = CSGBrushFactory.CompileHalfEdges(cubeverts);
		List<Polygon> cubepolys = CSGBrushFactory.CompilePolygons(cubeedges, cube.GetComponent<MeshRenderer>().material);

		//Material assignment (temporary)
		foreach (Polygon p in cubepolys) {
			p.material = cube.GetComponent<MeshRenderer>().material;
		}

		Surface cubeSurf = new Surface(cubepolys);

		List<Polygon> testtope = polys;
		Surface testSurf = new Surface(testtope);
		List<Surface> surfs = new List<Surface>() { testSurf };
		//*/

		/*
		List<Polygon> interior = new List<Polygon>();
		List<Polygon> exterior = new List<Polygon>();

		List<Polygon> ext1 = new List<Polygon>();
		List<Polygon> ext2 = new List<Polygon>();
		List<Polygon> ext3 = new List<Polygon>();
		List<Polygon> ext4 = new List<Polygon>();
		List<Polygon> ext5 = new List<Polygon>();

		/*
		CSGBrushComparator.splitSurfaceByPlane(testSurf, cubeplanes[0], out interior, out exterior);
		
		Surface intSurf = new Surface(CSGBrushComparator.CreateDuplicateInstances(interior));
		CSGBrushComparator.splitSurfaceByPlane(intSurf, cubeplanes[1], out interior, out ext1);

		Surface intSurf2 = new Surface(CSGBrushComparator.CreateDuplicateInstances(interior));
		CSGBrushComparator.splitSurfaceByPlane(intSurf2, cubeplanes[2], out interior, out ext2);

		Surface intSurf3 = new Surface(CSGBrushComparator.CreateDuplicateInstances(interior));
		CSGBrushComparator.splitSurfaceByPlane(intSurf3, cubeplanes[3], out interior, out ext3);

		Surface intSurf4 = new Surface(CSGBrushComparator.CreateDuplicateInstances(interior));
		CSGBrushComparator.splitSurfaceByPlane(intSurf4, cubeplanes[4], out interior, out ext4);
		
		Surface intSurf5 = new Surface(CSGBrushComparator.CreateDuplicateInstances(interior));
		CSGBrushComparator.splitSurfaceByPlane(intSurf5, cubeplanes[5], out interior, out ext5);
		//*/

		/*
		CSGBrushComparator.splitSurfaceByPlane(cubeSurf, testSurf.polys[0].plane, out interior, out exterior);
		
		Surface intCube = new Surface(CSGBrushComparator.CreateDuplicateInstances(interior));
		CSGBrushComparator.splitSurfaceByPlane(intCube, testSurf.polys[1].plane, out interior, out ext1);

		Surface intCube2 = new Surface(CSGBrushComparator.CreateDuplicateInstances(interior));
		CSGBrushComparator.splitSurfaceByPlane(intCube2, testSurf.polys[4].plane, out interior, out ext2);

		Surface intCube3 = new Surface(CSGBrushComparator.CreateDuplicateInstances(interior));
		CSGBrushComparator.splitSurfaceByPlane(intCube3, testSurf.polys[5].plane, out interior, out ext3);

		Surface intCube4 = new Surface(CSGBrushComparator.CreateDuplicateInstances(interior));
		CSGBrushComparator.splitSurfaceByPlane(intCube4, testSurf.polys[15].plane, out interior, out ext4);
		
		Surface intCube5 = new Surface(CSGBrushComparator.CreateDuplicateInstances(interior));
		CSGBrushComparator.splitSurfaceByPlane(intCube5, testSurf.polys[7].plane, out interior, out ext5);
		//*/

		//exterior.AddRange(ext1);
		//exterior.AddRange(ext2);
		//exterior.AddRange(ext3);
		//exterior.AddRange(ext4);
		//exterior.AddRange(ext5);

		/*
		GameObject gin = new GameObject();
		gin.name = "Interior";

		foreach (Polygon p in cubepolys) {
			GameObject poly = new GameObject();
			poly.name = "Polygon";
			poly.transform.SetParent(gin.transform);

			foreach (HalfEdge h in p.hedges) {
				GameObject ep = new GameObject();
				ep.name = "Edge Pair";
				ep.transform.position = h.start.position + h.end.position;
				ep.transform.SetParent(poly.transform);

				GameObject he = GameObject.CreatePrimitive(PrimitiveType.Cube);
				he.name = "h";
				he.transform.position = h.start.position;
				he.transform.localScale *= 0.05f;
				he.GetComponent<MeshRenderer>().material = Resources.Load<Material>("Materials/Blue");
				he.transform.SetParent(ep.transform);

				GameObject te = GameObject.CreatePrimitive(PrimitiveType.Sphere);
				te.name = "t";
				te.transform.position = h.next.start.position; //h.twin.start.position does not work
				te.transform.localScale *= 0.0675f;
				te.GetComponent<MeshRenderer>().material = Resources.Load<Material>("Materials/Green");
				te.transform.SetParent(ep.transform);
			}
		}

		GameObject gout = new GameObject();
		gout.name = "Exterior";

		foreach (Polygon p in exterior) {
			GameObject poly = new GameObject();
			poly.name = "Polygon";
			poly.transform.SetParent(gout.transform);

			foreach (HalfEdge h in p.hedges) {
				GameObject ep = new GameObject();
				ep.name = "Edge Pair";
				ep.transform.position = h.start.position + h.end.position;
				ep.transform.SetParent(poly.transform);

				GameObject he = GameObject.CreatePrimitive(PrimitiveType.Cube);
				he.name = "h";
				he.transform.position = h.start.position;
				he.transform.localScale *= 0.05f;
				he.GetComponent<MeshRenderer>().material = Resources.Load<Material>("Materials/Blue");
				he.transform.SetParent(ep.transform);

				GameObject te = GameObject.CreatePrimitive(PrimitiveType.Sphere);
				te.name = "t";
				te.transform.position = h.twin.start.position;
				te.transform.localScale *= 0.0675f;
				te.GetComponent<MeshRenderer>().material = Resources.Load<Material>("Materials/Green");
				te.transform.SetParent(ep.transform);
			}
		}
		//*
		GameObject gout1 = new GameObject();
		gout1.name = "Ext1";

		foreach (Polygon p in ext1) {
			GameObject poly = new GameObject();
			poly.name = "Polygon";
			poly.transform.SetParent(gout1.transform);

			foreach (HalfEdge h in p.hedges) {
				GameObject ep = new GameObject();
				ep.name = "Edge Pair";
				ep.transform.position = h.start.position + h.end.position;
				ep.transform.SetParent(poly.transform);

				GameObject he = GameObject.CreatePrimitive(PrimitiveType.Cube);
				he.name = "h";
				he.transform.position = h.start.position;
				he.transform.localScale *= 0.05f;
				he.GetComponent<MeshRenderer>().material = Resources.Load<Material>("Materials/Blue");
				he.transform.SetParent(ep.transform);

				GameObject te = GameObject.CreatePrimitive(PrimitiveType.Sphere);
				te.name = "t";
				te.transform.position = h.next.start.position;
				te.transform.localScale *= 0.0675f;
				te.GetComponent<MeshRenderer>().material = Resources.Load<Material>("Materials/Green");
				te.transform.SetParent(ep.transform);
			}
		}

		GameObject gout2 = new GameObject();
		gout2.name = "Ext2";

		foreach (Polygon p in ext2) {
			GameObject poly = new GameObject();
			poly.name = "Polygon";
			poly.transform.SetParent(gout2.transform);

			foreach (HalfEdge h in p.hedges) {
				GameObject ep = new GameObject();
				ep.name = "Edge Pair";
				ep.transform.position = h.start.position + h.end.position;
				ep.transform.SetParent(poly.transform);

				GameObject he = GameObject.CreatePrimitive(PrimitiveType.Cube);
				he.name = "h";
				he.transform.position = h.start.position;
				he.transform.localScale *= 0.05f;
				he.GetComponent<MeshRenderer>().material = Resources.Load<Material>("Materials/Blue");
				he.transform.SetParent(ep.transform);

				GameObject te = GameObject.CreatePrimitive(PrimitiveType.Sphere);
				te.name = "t";
				te.transform.position = h.next.start.position;
				te.transform.localScale *= 0.0675f;
				te.GetComponent<MeshRenderer>().material = Resources.Load<Material>("Materials/Green");
				te.transform.SetParent(ep.transform);
			}
		}

		GameObject gout3 = new GameObject();
		gout3.name = "Ext3";

		foreach (Polygon p in ext3) {
			GameObject poly = new GameObject();
			poly.name = "Polygon";
			poly.transform.SetParent(gout3.transform);

			foreach (HalfEdge h in p.hedges) {
				GameObject ep = new GameObject();
				ep.name = "Edge Pair";
				ep.transform.position = h.start.position + h.end.position;
				ep.transform.SetParent(poly.transform);

				GameObject he = GameObject.CreatePrimitive(PrimitiveType.Cube);
				he.name = "h";
				he.transform.position = h.start.position;
				he.transform.localScale *= 0.05f;
				he.GetComponent<MeshRenderer>().material = Resources.Load<Material>("Materials/Blue");
				he.transform.SetParent(ep.transform);

				GameObject te = GameObject.CreatePrimitive(PrimitiveType.Sphere);
				te.name = "t";
				te.transform.position = h.next.start.position;
				te.transform.localScale *= 0.0675f;
				te.GetComponent<MeshRenderer>().material = Resources.Load<Material>("Materials/Green");
				te.transform.SetParent(ep.transform);
			}
		}

		GameObject gout4 = new GameObject();
		gout4.name = "Ext4";

		foreach (Polygon p in ext4) {
			GameObject poly = new GameObject();
			poly.name = "Polygon";
			poly.transform.SetParent(gout4.transform);

			foreach (HalfEdge h in p.hedges) {
				GameObject ep = new GameObject();
				ep.name = "Edge Pair";
				ep.transform.position = h.start.position + h.end.position;
				ep.transform.SetParent(poly.transform);

				GameObject he = GameObject.CreatePrimitive(PrimitiveType.Cube);
				he.name = "h";
				he.transform.position = h.start.position;
				he.transform.localScale *= 0.05f;
				he.GetComponent<MeshRenderer>().material = Resources.Load<Material>("Materials/Blue");
				he.transform.SetParent(ep.transform);

				GameObject te = GameObject.CreatePrimitive(PrimitiveType.Sphere);
				te.name = "t";
				te.transform.position = h.next.start.position;
				te.transform.localScale *= 0.0675f;
				te.GetComponent<MeshRenderer>().material = Resources.Load<Material>("Materials/Green");
				te.transform.SetParent(ep.transform);
			}
		}

		GameObject gout5 = new GameObject();
		gout5.name = "Ext5";

		foreach (Polygon p in ext5) {
			GameObject poly = new GameObject();
			poly.name = "Polygon";
			poly.transform.SetParent(gout5.transform);

			foreach (HalfEdge h in p.hedges) {
				GameObject ep = new GameObject();
				ep.name = "Edge Pair";
				ep.transform.position = h.start.position + h.end.position;
				ep.transform.SetParent(poly.transform);

				GameObject he = GameObject.CreatePrimitive(PrimitiveType.Cube);
				he.name = "h";
				he.transform.position = h.start.position;
				he.transform.localScale *= 0.05f;
				he.GetComponent<MeshRenderer>().material = Resources.Load<Material>("Materials/Blue");
				he.transform.SetParent(ep.transform);

				GameObject te = GameObject.CreatePrimitive(PrimitiveType.Sphere);
				te.name = "t";
				te.transform.position = h.next.start.position;
				te.transform.localScale *= 0.0675f;
				te.GetComponent<MeshRenderer>().material = Resources.Load<Material>("Materials/Green");
				te.transform.SetParent(ep.transform);
			}
		}
		//*/

		/*
		List<HalfEdge> thes = new List<HalfEdge>();
		thes.Add(new HalfEdge(new Vector3(-0.2268f, -0.5f, 0.26762f)));
		thes.Add(new HalfEdge(new Vector3(-0.5f, 0.21118f, -0.5f)));
		thes.Add(new HalfEdge(new Vector3(-0.0732f, -0.5f, -0.5f)));
		thes.Add(new HalfEdge(new Vector3(-0.5f, 0.01407f, 0.09133f)));

		Plane theplane = new Plane(thes[0].start, thes[1].start, thes[2].start);
		Polygon thepoly = new Polygon(theplane);

		List<HalfEdge> sthes = CSGBrushFactory.SortHalfEdges(thes, thepoly);

		foreach(HalfEdge h in sthes) {
			GameObject g = GameObject.CreatePrimitive(PrimitiveType.Cube);
			g.transform.position = h.start;
			g.transform.localScale = Vector3.one * 0.1f;
		}
		//*/

		/*
		cube.AddComponent<CSGPrimitiveBrush>();
		CSGPrimitiveBrush cubebrush = cube.GetComponent<CSGPrimitiveBrush>();
		cubebrush.surfaces[cube] = new List<Surface> { cubeSurf };

		cylinder.AddComponent<CSGPrimitiveBrush>();
		CSGPrimitiveBrush cylinderbrush = cylinder.GetComponent<CSGPrimitiveBrush>();
		cylinderbrush.surfaces[cylinder] = new List<Surface> { testSurf };
		//*/

		GameObject cube2 = new GameObject();
		cube2.AddComponent<CSGCubeBrush>();
		CSGCubeBrush cube2brush = cube2.GetComponent<CSGCubeBrush>();
		cube2brush.InstantiateBrush();

		GameObject prism = new GameObject();
		prism.AddComponent<CSGPrismBrush>();
		CSGPrismBrush prismbrush = prism.GetComponent<CSGPrismBrush>();
		prismbrush.InstantiateBrush();

		GameObject pyramid = new GameObject();
		pyramid.AddComponent<CSGPyramidBrush>();
		CSGPyramidBrush pyramidbrush = pyramid.GetComponent<CSGPyramidBrush>();
		//
		pyramidbrush.radius = 0.6f;
		pyramidbrush.height = 2;
		pyramidbrush.sides = 8;
		//
		pyramidbrush.InstantiateBrush();

		GameObject derived = new GameObject();
		derived.AddComponent<CSGDerivativeBrush>();
		CSGDerivativeBrush derivedbrush = derived.GetComponent<CSGDerivativeBrush>();
		derivedbrush.InstantiateBrush(cube2brush, prismbrush, Operation.BnotA);
		derivedbrush.enabled = false;
		derivedbrush.enabled = true;

		//*
		GameObject sphere = new GameObject();
		sphere.AddComponent<CSGSphereBrush>();
		CSGSphereBrush spherebrush = sphere.GetComponent<CSGSphereBrush>();
		spherebrush.InstantiateBrush();

		GameObject derived2 = new GameObject();
		derived2.AddComponent<CSGDerivativeBrush>();
		CSGDerivativeBrush derived2brush = derived2.GetComponent<CSGDerivativeBrush>();
		derived2brush.InstantiateBrush(pyramidbrush, derivedbrush, Operation.AorB);
		derived2brush.enabled = false;
		derived2brush.enabled = true;
		//*/

		//

		//NOTE: CSGBrushes get screwy when rotated in multiple directions/to extremes
		// -> Check split functions (apply modulus?)
		// -> Other brush seemingly moves away

		/*
		cylinder.transform.position = Vector3.zero; // new Vector3(0.3f, -0.2f, 0.5f);
		cylinder.transform.up = Vector3.up; // Vector3(1, 0.5f, -1);

		cube.transform.position = new Vector3(0.3f, 0.0f, 0.75f);
		cube.transform.up = new Vector3(.5f, -.3f, 0.1f); // new Vector3(-0.3f, 0.3f, 0.9f);

		cylinder.transform.localScale = Vector3.one * 1.1f;
		cube.transform.localScale = Vector3.one * 1.5f;
		//*/

		/*
		Quaternion rotationCylinder = 
			Quaternion.Inverse(cylinder.transform.rotation) * 
			cube.transform.rotation;
		Quaternion rotationCube = 
			Quaternion.Inverse(cube.transform.rotation) * 
			cylinder.transform.rotation;

		Vector3 translationCylinder = 
			Quaternion.FromToRotation(cylinder.transform.up, Vector3.up) * 
			(cylinder.transform.position - cube.transform.position);
		Vector3 translationCube = 
			Quaternion.FromToRotation(cube.transform.up, Vector3.up) * 
			(cube.transform.position - cylinder.transform.position);
		
		// Works for proportional scaling (Vector3.one * f) only; unclear why
		translationCylinder = new Vector3(
			translationCylinder.x / cylinder.transform.localScale.x,
			translationCylinder.y / cylinder.transform.localScale.y,
			translationCylinder.z / cylinder.transform.localScale.z);

		translationCube = new Vector3(
			translationCube.x / cube.transform.localScale.x,
			translationCube.y / cube.transform.localScale.y,
			translationCube.z / cube.transform.localScale.z);

		Vector3 scaleCylinder = new Vector3(
			cube.transform.localScale.x / cylinder.transform.localScale.x,
			cube.transform.localScale.y / cylinder.transform.localScale.y,
			cube.transform.localScale.z / cylinder.transform.localScale.z);
		Vector3 scaleCube = new Vector3(
			cylinder.transform.localScale.x / cube.transform.localScale.x,
			cylinder.transform.localScale.y / cube.transform.localScale.y,
			cylinder.transform.localScale.z / cube.transform.localScale.z);

		//* Surface by Surface Split
		Surface intsurf;
		List<Surface> extsurfs;
		CSGBrushComparator.splitSurfaceBySurface(testSurf, cubeSurf, translationCylinder, rotationCylinder, scaleCylinder, out intsurf, out extsurfs);

		Surface intsurf2;
		List<Surface> extsurfs2;
		CSGBrushComparator.splitSurfaceBySurface(cubeSurf, testSurf, translationCube, rotationCube, scaleCube, out intsurf2, out extsurfs2);

		// Subsurface cuts seems to work (at least partially), but NOT WHEN FLUSH

		Surface intsurf3;
		List<Surface> intsurfs3;
		List<Surface> extsurfs3;
		Surface intsurf4;
		List<Surface> intsurfs4;
		List<Surface> extsurfs4;
		//CSGBrushComparator.splitSurfaceBySurface(testSurf, extsurfs[2], new Vector3(1, 1, 1) * 0.3f * 1, Quaternion.identity, out intsurf3, out extsurfs3);
		//CSGBrushComparator.splitSurfaceBySurface(extsurfs[2], testSurf, new Vector3(1, 1, 1) * 0.3f * -1, Quaternion.identity, out intsurf4, out extsurfs4);
		//*/

		//

		/*
		//NOTE: 
		// Mesh holes seem to only appear on internal faces, 
		// presumbly never exceed one triangle per surface cut, 
		// and increase in likelihood on consecutive cuts 

		Brush cylinderBrush = new PrimitiveBrush(testSurf, cylinder);
		Brush cubeBrush = new PrimitiveBrush(cubeSurf, cube);

		Brush combined = new DerivativeBrush(cylinderBrush, cubeBrush, Operation.AorB);

		//*
		GameObject dupe = Instantiate(cube);

		//Keep scale at 1 for testing (not yet functional for DerivativeBrushes)
		dupe.transform.localScale = Vector3.one * 1.2f;
		dupe.transform.position = new Vector3(-.35f, 0, .5f);
		dupe.transform.up = Vector3.up;

		//Polygon materials need updated in PrimitiveBrush on gameObject change
		dupe.GetComponent<MeshRenderer>().material = Resources.Load<Material>("Materials/Blue");
		foreach (Polygon p in cubeSurf.polys) {
			p.material = dupe.GetComponent<MeshRenderer>().material;
		}

		Brush dupeBrush = new PrimitiveBrush(new Surface(CSGBrushComparator.DuplicateInstances(cubeSurf.polys)), dupe);
		foreach (Polygon p in cubeSurf.polys) {
			p.material = cube.GetComponent<MeshRenderer>().material;
		}

		//combined.gameObject.transform.localScale = Vector3.one * 0.9f;
		//combined.gameObject.transform.up = new Vector3(0.9f, -.7f, -.3f);

		Brush negative = new DerivativeBrush(dupeBrush, combined, Operation.BnotA);
		//NOTE: 'negative' brush FAILS unexpectedly when 'combined' operation is BnotA
		//*/

		/*
		//DISCOVERIES:
		// MESH HOLES CAUSED BY MISSING HALFEDGES
		// MESH HOLES SEEM TO APPEAR ON INTERNAL FACES (CREATED BY PLANEIN/PLANEOUT)

		foreach(GameObject g in negative.surfaces.Keys) {
			foreach(Surface s in negative.surfaces[g]) {
				GameObject surf = new GameObject();
				surf.transform.SetParent(g.transform);
				surf.name = "Surface";
				foreach(Polygon p in s.polys) {
					GameObject poly = new GameObject();
					poly.transform.SetParent(surf.transform);
					poly.name = "Polygon";
					foreach(HalfEdge h in p.hedges) {
						GameObject ep = new GameObject();
						ep.name = "Edge Pair";
						ep.transform.position = h.start + h.next.start;
						ep.transform.SetParent(poly.transform);

						GameObject he = GameObject.CreatePrimitive(PrimitiveType.Cube);
						he.name = "h";
						he.transform.position = h.start;
						he.transform.localScale *= 0.05f;
						he.GetComponent<MeshRenderer>().material.SetColor("Color", Color.magenta);
						he.transform.SetParent(ep.transform);

						GameObject te = GameObject.CreatePrimitive(PrimitiveType.Sphere);
						te.name = "t";
						te.transform.position = h.next.start;
						te.transform.localScale *= 0.0675f;
						te.GetComponent<MeshRenderer>().material.SetColor("Color", Color.green);
						te.transform.SetParent(ep.transform);
					}
				}
			}
		}
		//*/

		//

		// WIP -> Serialization Testing
		/*
		GameObject scriptcube = new GameObject();
		scriptcube.AddComponent<CSGDerivativeBrush>();
		CSGDerivativeBrush script = scriptcube.GetComponent<CSGDerivativeBrush>();
		script.a = cubeBrush;
		script.b = cylinderBrush;
		script.operation = Operation.AandB;
		//*/

		//

		//CSGBrushComparator.splitSurfacesBySurfaces(new List<Surface>() { testSurf }, extsurfs, Vector3.zero, Quaternion.identity, out intsurfs3, out extsurfs3);
		//Debug.Log("OI: " + intsurfs3.Count + " | " + extsurfs3.Count);

		/* Addition (A or B)
		Mesh testmesh = CSGBrushComparator.RenderExteriorSurfaces(extsurfs);
		Mesh testmesh2 = CSGBrushComparator.RenderExteriorSurfaces(extsurfs2);
		//*/

		/* Subtraction (A not B)
		Mesh testmesh = CSGBrushComparator.RenderExteriorSurfaces(extsurfs);
		Mesh testmesh2 = CSGBrushComparator.RenderInteriorSurfaces(new List<Surface>() { intsurf2 });
		//*/

		/* Subtraction (B not A)
		Mesh testmesh = CSGBrushComparator.RenderInteriorSurfaces(new List<Surface>() { intsurf });
		Mesh testmesh2 = CSGBrushComparator.RenderExteriorSurfaces(extsurfs2);
		//*/

		/* Intersection (A and B)
		Mesh testmesh = CSGBrushComparator.RenderExteriorSurfaces(new List<Surface>() { intsurf } );
		Mesh testmesh2 = CSGBrushComparator.RenderExteriorSurfaces(new List<Surface>() { intsurf2 });
		//*/

		/* Simultaneous Subtraction (with Intersection)
		Mesh testmesh3 = CSGBrushComparator.RenderExteriorSurfaces(extsurfs);
		Mesh testmesh4 = CSGBrushComparator.RenderInteriorSurfaces(new List<Surface>() { intsurf2 });

		cylinder.GetComponent<MeshFilter>().mesh.Clear();
		cylinder.GetComponent<MeshFilter>().mesh = testmesh3;

		cube.GetComponent<MeshFilter>().mesh.Clear();
		cube.GetComponent<MeshFilter>().mesh = testmesh4;
		//*/

		/*
		GameObject ag = GameObject.CreatePrimitive(PrimitiveType.Cube);
		Mesh amesh = ag.GetComponent<MeshFilter>().mesh;
		amesh.Clear();
		amesh.vertices = testmesh.vertices;
		amesh.uv = testmesh.uv;
		amesh.triangles = testmesh.triangles;

		GameObject bg = GameObject.CreatePrimitive(PrimitiveType.Cube);
		Mesh bmesh = bg.GetComponent<MeshFilter>().mesh;
		bmesh.Clear();
		bmesh.vertices = testmesh2.vertices;
		bmesh.uv = testmesh2.uv;
		bmesh.triangles = testmesh2.triangles;
		//*/

		/*
		Debug.Log(cylinder.GetComponent<MeshFilter>().mesh.vertices.Count());
		Debug.Log(cylinder.GetComponent<MeshFilter>().mesh.vertices.Distinct().Count());

		GameObject ag = new GameObject("Brush A");
		ag.AddComponent<MeshFilter>();
		ag.GetComponent<MeshFilter>().mesh = new Mesh();
		ag.GetComponent<MeshFilter>().mesh.Clear();
		ag.GetComponent<MeshFilter>().mesh.vertices = testmesh.vertices;
		ag.GetComponent<MeshFilter>().mesh.uv = testmesh.uv;//CSGBrushComparator.RecalculateUVs(testmesh.vertices, cylinder);
		ag.GetComponent<MeshFilter>().mesh.triangles = testmesh.triangles;
		ag.GetComponent<MeshFilter>().mesh.normals = testmesh.normals;
		ag.AddComponent<MeshRenderer>();
		ag.GetComponent<MeshRenderer>().material.SetColor("_Color", Color.red);

		//Align with Brush game object
		ag.transform.position = cylinder.transform.position;
		ag.transform.rotation = cylinder.transform.rotation;
		ag.transform.localScale = cylinder.transform.localScale;

		GameObject bg = new GameObject("Brush B");
		bg.AddComponent<MeshFilter>();
		bg.GetComponent<MeshFilter>().mesh = new Mesh();
		bg.GetComponent<MeshFilter>().mesh.Clear();
		bg.GetComponent<MeshFilter>().mesh.vertices = testmesh2.vertices;
		bg.GetComponent<MeshFilter>().mesh.uv = testmesh2.uv;
		bg.GetComponent<MeshFilter>().mesh.triangles = testmesh2.triangles;
		bg.GetComponent<MeshFilter>().mesh.normals = testmesh2.normals;
		bg.AddComponent<MeshRenderer>();
		bg.GetComponent<MeshRenderer>().material.SetColor("_Color", Color.white);

		bg.transform.position = cube.transform.position;
		bg.transform.rotation = cube.transform.rotation;
		bg.transform.localScale = cube.transform.localScale;

		GameObject sin = new GameObject();
		sin.name = "Interior Surface";

		foreach (Polygon p in intsurf.polys) {
			GameObject poly = new GameObject();
			poly.name = "Polygon";
			poly.transform.SetParent(sin.transform);

			foreach (HalfEdge h in p.hedges) {
				GameObject ep = new GameObject();
				ep.name = "Edge Pair";
				ep.transform.position = h.start + h.next.start;
				ep.transform.SetParent(poly.transform);

				GameObject he = GameObject.CreatePrimitive(PrimitiveType.Cube);
				he.name = "h";
				he.transform.position = h.start;
				he.transform.localScale *= 0.05f;
				he.GetComponent<MeshRenderer>().material = Resources.Load<Material>("Materials/Blue");
				he.transform.SetParent(ep.transform);

				GameObject te = GameObject.CreatePrimitive(PrimitiveType.Sphere);
				te.name = "t";
				te.transform.position = h.next.start;
				te.transform.localScale *= 0.0675f;
				te.GetComponent<MeshRenderer>().material = Resources.Load<Material>("Materials/Green");
				te.transform.SetParent(ep.transform);
			}
		}

		GameObject souts = new GameObject();
		souts.name = "Exterior Surfaces";

		foreach (Surface s in extsurfs) {

			GameObject sout = new GameObject();
			sout.name = "Exterior Surface";
			sout.transform.SetParent(souts.transform);

			foreach (Polygon p in s.polys) {
				GameObject poly = new GameObject();
				poly.name = "Polygon";
				poly.transform.SetParent(sout.transform);

				foreach (HalfEdge h in p.hedges) {
					GameObject ep = new GameObject();
					ep.name = "Edge Pair";
					ep.transform.position = h.start + h.next.start;
					ep.transform.SetParent(poly.transform);

					GameObject he = GameObject.CreatePrimitive(PrimitiveType.Cube);
					he.name = "h";
					he.transform.position = h.start;
					he.transform.localScale *= 0.05f;
					he.GetComponent<MeshRenderer>().material = Resources.Load<Material>("Materials/Blue");
					he.transform.SetParent(ep.transform);

					GameObject te = GameObject.CreatePrimitive(PrimitiveType.Sphere);
					te.name = "t";
					te.transform.position = h.next.start;
					te.transform.localScale *= 0.0675f;
					te.GetComponent<MeshRenderer>().material = Resources.Load<Material>("Materials/Green");
					te.transform.SetParent(ep.transform);
				}
			}
		}

		//*/

		/* Obsolete
		HalfEdge[] heds = CSGBrushFactory.CompileHalfEdges(plys).ToArray();

        foreach (HalfEdge h in heds) {

            if (h.poly == plys[3]) {
                GameObject ep = new GameObject();
                ep.name = "Edge Pair";
                ep.transform.position = h.start.position + h.end.position;

                GameObject he = GameObject.CreatePrimitive(PrimitiveType.Cube);
                he.name = "h";
                he.transform.position = h.start.position;
                he.transform.localScale *= 0.05f;
                he.GetComponent<MeshRenderer>().material = Resources.Load<Material>("Materials/Blue");
                he.transform.SetParent(ep.transform);

                GameObject te = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                te.name = "t";
                te.transform.position = h.twin.start.position;
                te.transform.localScale *= 0.0675f;
                te.GetComponent<MeshRenderer>().material = Resources.Load<Material>("Materials/Green");
                te.transform.SetParent(ep.transform);
            }
        }
        //*/
	}

	// Update is called once per frame
	void Update () {
	
	}
}
