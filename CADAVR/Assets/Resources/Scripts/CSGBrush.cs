﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[System.Serializable]
public abstract class CSGBrush : MonoBehaviour {

	public Dictionary<GameObject, List<Surface>> surfaces = new Dictionary<GameObject, List<Surface>>();

	public delegate void UpdateEvent();
	public event UpdateEvent OnUpdate;

	// Move OnUpdate into RebuildBrush()?

	private void Update() {
		// Not ideal, but functional
		if (transform.hasChanged) {
			if (OnUpdate != null) {
				OnUpdate();
			}
			transform.hasChanged = false;
		}
	}

	private void OnValidate() {
		RebuildBrush();
		if (OnUpdate != null) {
			OnUpdate();
		}
	}

	protected abstract void RebuildBrush();
}

public enum Type { Cube, Prism, Pyramid, Sphere }

public abstract class CSGPrimitiveBrush : CSGBrush {

	//public Type type;

	//Add serializable fields to subclasses
	//Cube    -> side length
	//Prism   -> height, inscribed radius, base shape (3 to 12?)
	//Sphere  -> Radius, LOD
	//Pyramid -> height, inscribed radius, base shape (3 to 12?)

	// gameObject is PrimitiveType
	// gameObject is empty with attached meshrenderer -> generate shapes



}

public class CSGCubeBrush : CSGPrimitiveBrush {

	//RADIUS UNNECESSARY WITH UNIFORM SCALING

	public void InstantiateBrush() {
		surfaces[gameObject] = new List<Surface>();
		gameObject.AddComponent<MeshRenderer>();
		gameObject.AddComponent<MeshFilter>();
		gameObject.name = "Cube Brush";
		RebuildBrush();
	}

	protected override void RebuildBrush() {
		List<Plane> planes = new List<Plane>();

		// Doesnt work with this overload syntax
		/*
		planes.Add(new Plane(Vector3.up, 0.5f));
		planes.Add(new Plane(-Vector3.up, 0.5f));
		planes.Add(new Plane(Vector3.right, 0.5f));
		planes.Add(new Plane(-Vector3.right, 0.5f));
		planes.Add(new Plane(Vector3.forward, 0.5f));
		planes.Add(new Plane(-Vector3.forward, 0.5f));
		//*/

		planes.Add(new Plane(Vector3.up, Vector3.up * 0.5f));
		planes.Add(new Plane(-Vector3.up, -Vector3.up * 0.5f));
		planes.Add(new Plane(Vector3.right, Vector3.right * 0.5f));
		planes.Add(new Plane(-Vector3.right, -Vector3.right * 0.5f));
		planes.Add(new Plane(Vector3.forward, Vector3.forward * 0.5f));
		planes.Add(new Plane(-Vector3.forward, -Vector3.forward * 0.5f));

		Dictionary<Vector3, List<Plane>> vertices = CSGBrushFactory.CompileVertices(planes);
		Dictionary<Plane, List<HalfEdge>> halfedges = CSGBrushFactory.CompileHalfEdges(vertices);
		List<Polygon> polygons = CSGBrushFactory.CompilePolygons(halfedges, gameObject.GetComponent<MeshRenderer>().material);
		Surface surface = new Surface(polygons);

		surfaces[gameObject] = new List<Surface> { surface };

		RenderMesh();
	}

	private void RenderMesh() {
		gameObject.GetComponent<MeshFilter>().mesh.Clear();
		gameObject.GetComponent<MeshFilter>().mesh = CSGBrushComparator.GenerateMesh(surfaces[gameObject], gameObject.GetComponent<MeshRenderer>().material);
	}
}

public class CSGPrismBrush : CSGPrimitiveBrush {

	public float radius = 0.5f; //inscribed
	public float height = 1f;
	public int sides = 8; // 3 to 16?

	public void InstantiateBrush() {
		surfaces[gameObject] = new List<Surface>();
		gameObject.AddComponent<MeshRenderer>();
		gameObject.AddComponent<MeshFilter>();
		gameObject.name = "Prism Brush";
		RebuildBrush();
	}

	protected override void RebuildBrush() {
		List<Plane> planes = new List<Plane>();
		planes.Add(new Plane(Vector3.up, Vector3.up * height * 0.5f));
		planes.Add(new Plane(-Vector3.up, -Vector3.up * height * 0.5f));

		//Add plane based on sides and radius
		Vector3 normal = Vector3.forward;
		float angle = 360f / sides;
		for(int i = 0; i < sides; i++) {
			planes.Add(new Plane(normal, normal * radius));
			normal = Quaternion.Euler(0, angle, 0) * normal;
		}

		Dictionary<Vector3, List<Plane>> vertices = CSGBrushFactory.CompileVertices(planes);
		Dictionary<Plane, List<HalfEdge>> halfedges = CSGBrushFactory.CompileHalfEdges(vertices);
		List<Polygon> polygons = CSGBrushFactory.CompilePolygons(halfedges, gameObject.GetComponent<MeshRenderer>().material);
		Surface surface = new Surface(polygons);

		surfaces[gameObject] = new List<Surface> { surface };

		RenderMesh();
	}

	private void RenderMesh() {
		gameObject.GetComponent<MeshFilter>().mesh.Clear();
		gameObject.GetComponent<MeshFilter>().mesh = CSGBrushComparator.GenerateMesh(surfaces[gameObject], gameObject.GetComponent<MeshRenderer>().material);
	}
}

public class CSGPyramidBrush : CSGPrimitiveBrush {

	public float radius = 0.5f; //inscribed (USE ESCRIBED INSTEAD?)
	public float height = 1f;
	public int sides = 8; // 3 to 16?

	public void InstantiateBrush() {
		surfaces[gameObject] = new List<Surface>();
		gameObject.AddComponent<MeshRenderer>();
		gameObject.AddComponent<MeshFilter>();
		gameObject.name = "Pyramid Brush";
		RebuildBrush();
	}

	protected override void RebuildBrush() {
		List<Plane> planes = new List<Plane>();
		planes.Add(new Plane(-Vector3.up, -Vector3.up * height * 0.5f));

		float phi = Mathf.Rad2Deg * Mathf.Atan(height / radius);
		float theta = 360f / sides;
		Vector3 normal = Quaternion.Euler(phi - 90, 0, 0) * Vector3.forward;

		//float inrad = radius * Mathf.Asin(theta * Mathf.Deg2Rad);
		float magnitude = Vector3.Magnitude((new Vector3(0, height * 0.5f, 0) + new Vector3(0, -height * 0.5f, radius)) / 2);

		for (int i = 0; i < sides; i++) {
			planes.Add(new Plane(normal, normal * magnitude));
			normal = Quaternion.Euler(0, theta, 0) * normal;
		}

		/*
		foreach(Plane p in planes) {
			GameObject g = GameObject.CreatePrimitive(PrimitiveType.Quad);
			g.transform.position = p.normal * -p.distance;
			g.transform.forward = p.normal;
			g.transform.localScale = Vector3.one * 0.1f;
		}
		//*/

		Dictionary<Vector3, List<Plane>> vertices = CSGBrushFactory.CompileVertices(planes);
		Dictionary<Plane, List<HalfEdge>> halfedges = CSGBrushFactory.CompileHalfEdges(vertices);
		List<Polygon> polygons = CSGBrushFactory.CompilePolygons(halfedges, gameObject.GetComponent<MeshRenderer>().material);
		Surface surface = new Surface(polygons);

		surfaces[gameObject] = new List<Surface> { surface };

		RenderMesh();
	}

	private void RenderMesh() {
		gameObject.GetComponent<MeshFilter>().mesh.Clear();
		gameObject.GetComponent<MeshFilter>().mesh = CSGBrushComparator.GenerateMesh(surfaces[gameObject], gameObject.GetComponent<MeshRenderer>().material);
	}
}

public class CSGSphereBrush : CSGPrimitiveBrush {

	//RADIUS UNNECESSARY WITH UNIFORM SCALING

	public int sides = 8; // 3 to 12?
	public int levels = 6; //3 to 8?

	public void InstantiateBrush() {
		surfaces[gameObject] = new List<Surface>();
		gameObject.AddComponent<MeshRenderer>();
		gameObject.AddComponent<MeshFilter>();
		gameObject.name = "Sphere Brush";
		RebuildBrush();
	}

	protected override void RebuildBrush() {
		List<Plane> planes = new List<Plane>();

		planes.Add(new Plane(Vector3.up, Vector3.up * 0.5f));
		planes.Add(new Plane(-Vector3.up, -Vector3.up * 0.5f));


		float phi = 180f / (levels + 1);
		float theta = 360f / sides;

		Vector3 normal = Quaternion.Euler(phi, 0, 0) * -Vector3.up;
		
		// WIP

		for (int i = 0; i < levels; i++) {
			for (int j = 0; j < sides; j++) {
				planes.Add(new Plane(normal, normal * 0.5f));
				normal = Quaternion.Euler(0, theta, 0) * normal;
			}
			normal = Quaternion.Euler(phi, 0, 0) * normal;
		}

		Dictionary<Vector3, List<Plane>> vertices = CSGBrushFactory.CompileVertices(planes);
		Dictionary<Plane, List<HalfEdge>> halfedges = CSGBrushFactory.CompileHalfEdges(vertices);
		List<Polygon> polygons = CSGBrushFactory.CompilePolygons(halfedges, gameObject.GetComponent<MeshRenderer>().material);
		Surface surface = new Surface(polygons);

		surfaces[gameObject] = new List<Surface> { surface };

		RenderMesh();
	}

	private void RenderMesh() {
		gameObject.GetComponent<MeshFilter>().mesh.Clear();
		gameObject.GetComponent<MeshFilter>().mesh = CSGBrushComparator.GenerateMesh(surfaces[gameObject], gameObject.GetComponent<MeshRenderer>().material);
	}
}

public class CSGDerivativeBrush : CSGBrush {

	public CSGBrush a;
	public CSGBrush b;

	private Dictionary<GameObject, List<Surface>>[] interiors = new Dictionary<GameObject, List<Surface>>[2];
	private Dictionary<GameObject, List<Surface>>[] exteriors = new Dictionary<GameObject, List<Surface>>[2];

	public Operation operation;

	// gameObject is Empty

	private void OnEnable() {
		if (a != null && b != null) {
			a.OnUpdate += RebuildBrush;
			b.OnUpdate += RebuildBrush;
		}
	}

	private void OnDisable() {
		a.OnUpdate -= RebuildBrush;
		b.OnUpdate -= RebuildBrush;
	}

	public void InstantiateBrush(CSGBrush a, CSGBrush b, Operation operation) {
		this.a = a;
		this.b = b;
		this.operation = operation;
		gameObject.name = "Derivative Brush";
		RebuildBrush();
	} 

	protected override void RebuildBrush() {
		DestroyGameObjects();
		CSGBrushComparator.splitBrushByBrush(a, b, out interiors[0], out exteriors[0]);
		CSGBrushComparator.splitBrushByBrush(b, a, out interiors[1], out exteriors[1]);
		ApplyOperation();
	}

	// Call ApplyOperation on change of operation 
	// NOTE: ApplyOperation cannot take Brush parameters (not stored) -> Needs fixed
	// Call new DerivativeBrush on change of Brush a or b

	private void ApplyOperation() {

		surfaces.Clear();

		if (operation == Operation.AorB) {
			surfaces = new Dictionary<GameObject, List<Surface>>();
			a.surfaces.ToList().ForEach(x => surfaces.Add(x.Key, x.Value));
			b.surfaces.ToList().ForEach(x => surfaces.Add(x.Key, x.Value));
		} else if (operation == Operation.AnotB) {
			surfaces = exteriors[0];
		} else if (operation == Operation.BnotA) {
			surfaces = exteriors[1];
		} else if (operation == Operation.AandB) {
			surfaces = interiors[0];
			//Add conditional to use mesh with fewer gameObjects?
		}

		GenerateGameObjects(surfaces);
	}

	private void DestroyGameObjects() {
		List<GameObject> gameObjects = surfaces.Keys.ToList();
		foreach (GameObject g in gameObjects) {
			Destroy(g);
		}
	}

	private void GenerateGameObjects(Dictionary<GameObject, List<Surface>> surfaces) {

		List<GameObject> gameObjects = surfaces.Keys.ToList();
		foreach (GameObject g in gameObjects) {
			CompileGameObjects(surfaces[g], g);
		}
	}

	// NOTE: Renders unnecessary internal faces for all operations except Intersect
	private void CompileGameObjects(List<Surface> subsurfaces, GameObject reference) {

		Dictionary<Material, List<Polygon>> dictionary = new Dictionary<Material, List<Polygon>>();

		//Exclude internal polygons unless s in last surface in surfaces?
		//How to differentiate between external and internal?
		foreach (Surface s in subsurfaces) {
			foreach (Polygon p in s.polys) {
				if (!dictionary.ContainsKey(p.material)) {
					dictionary[p.material] = new List<Polygon>();
				}
				dictionary[p.material].Add(p);
			}
		}

		GameObject g = reference;

		foreach (Material m in dictionary.Keys) {
			g = new GameObject();

			g.AddComponent<MeshRenderer>();
			g.GetComponent<MeshRenderer>().material = m;

			g.AddComponent<MeshFilter>();
			g.GetComponent<MeshFilter>().mesh = CSGBrushComparator.GenerateMesh(subsurfaces, m);

			g.transform.position = reference.transform.position; // + gameObject.transform.position;
			g.transform.rotation = reference.transform.rotation; // * gameObject.transform.rotation;
			g.transform.localScale = reference.transform.localScale;
			g.transform.SetParent(gameObject.transform);

			//Add gameObject to surfaces list -> necessary to delete on update
			surfaces[g] = new List<Surface>();
		}

		//Rebind surfaces to one of the generated gameObjects (child of Brush gameObject)
		surfaces[g] = surfaces[reference];
		surfaces.Remove(reference);
	}
}
