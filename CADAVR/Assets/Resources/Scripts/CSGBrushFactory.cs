﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

public class CSGBrushFactory : MonoBehaviour {

	// Use this for initialization
	void Start() {

	}

	// Update is called once per frame
	void Update() {

	}

	// PROSPECTIVE LAYOUT
	// Prefabs
	// Creation
	// Instantiation

	// BRUSH CREATION (for undefined shapes)

	// Checks if all three planes intersect at some point in space (no parallel planes)
	public static bool CheckPlaneIntersection(Plane a, Plane b, Plane c, out float abcDeterminant) {

		abcDeterminant =
			(a.normal.x * (b.normal.y * c.normal.z - b.normal.z * c.normal.y)
			- a.normal.y * (b.normal.x * c.normal.z - b.normal.z * c.normal.x)
			+ a.normal.z * (b.normal.x * c.normal.y - b.normal.y * c.normal.x));

		if (abcDeterminant == 0) {
			return false;
		} else {
			return true;
		}
	}

	// Finds the point of intersection of three planes in space
	public static Vector3 FindPlaneIntersection(Plane a, Plane b, Plane c, float abcDeterminant) {

		float aDot = Vector3.Dot(a.normal, (a.normal * -a.distance));
		float bDot = Vector3.Dot(b.normal, (b.normal * -b.distance));
		float cDot = Vector3.Dot(c.normal, (c.normal * -c.distance));

		Vector3 abCross = Vector3.Cross(a.normal, b.normal);
		Vector3 bcCross = Vector3.Cross(b.normal, c.normal);
		Vector3 caCross = Vector3.Cross(c.normal, a.normal);

		Vector3 abcIntersect = (1 / abcDeterminant)
			* ((aDot * bcCross) + (bDot * caCross) + (cDot * abCross));

		return abcIntersect;
	}

	// REFACTOR THIS BLOCK

	public static Dictionary<Vector3, List<Plane>> CompileVertices(List<Plane> planes) {

		Dictionary<Vector3, List<Plane>> vertices = new Dictionary<Vector3, List<Plane>>();
		planes = planes.Distinct().ToList(); //Ensure planes contains no duplicates

		// Iterate over every three plane combination in list
		for (int i = 0; i < planes.Count - 2; i++) {
			for (int j = i + 1; j < planes.Count - 1; j++) {
				for (int k = j + 1; k < planes.Count; k++) {
					float determinant;
					if (CheckPlaneIntersection(planes[i], planes[j], planes[k], out determinant)) {

						Vector3 vertex = FindPlaneIntersection(planes[i], planes[j], planes[k], determinant);

						//Check if vert:
						// is unique (not already stored; update contacting planes appropriately)
						// is valid (on polytope)

						List<Plane> vertexPlanes = new List<Plane>();
						if (!vertices.ContainsKey(vertex)) {

							//Check if vertex is contained by every plane
							//If so, add plane to vertex's local plane list
							foreach (Plane p in planes) {
								if (p.GetDistanceToPoint(vertex) > 0.001f) { //p.GetSide(vert.position) is inaccurate
									vertexPlanes.Clear();
									break;
								} else if (Mathf.Abs(Vector3.Dot(p.normal, (p.normal * -p.distance) - vertex)) < 0.001f) {
									vertexPlanes.Add(p);
								}
							}
						}

						//If criteria are met, add vertex to vertices dictionary
						if (vertexPlanes.Count > 0) {
							vertices[vertex] = vertexPlanes;
						}
					}
				}
			}
		}

		return vertices;
	}

	//Maintain original functionality? -> compile unsorted list of vector3s, then sort?

	//*
	public static Dictionary<Plane, List<HalfEdge>> CompileHalfEdges(Dictionary<Vector3, List<Plane>> vertices) {
		
		Dictionary<Plane, List<HalfEdge>> halfedges = new Dictionary<Plane, List<HalfEdge>>();

		foreach (KeyValuePair<Vector3, List<Plane>> entry in vertices) {
			foreach (Plane p in entry.Value) {
				HalfEdge h = new HalfEdge(entry.Key);
				if (halfedges.ContainsKey(p)) {
					halfedges[p].Add(h);
				} else {
					halfedges[p] = new List<HalfEdge> { h };
				}
			}
		}

		return halfedges;
	}

	public static List<Polygon> CompilePolygons(Dictionary<Plane, List<HalfEdge>> halfedges, Material material) {

		List<Polygon> polygons = new List<Polygon>();

		foreach (KeyValuePair<Plane, List<HalfEdge>> entry in halfedges) {

			Polygon p = new Polygon(entry.Key, material);
			p.hedges = SortHalfEdges(entry.Value, p);

			polygons.Add(p);
		}

		BindSerialHalfEdges(polygons);
		BindAntiparallelHalfEdges(polygons);

		return polygons;
	}
	//*/


	//NOTE: STILL NOT FULLY FUNCTIONAL -> TOP PRIORITY
	public static List<HalfEdge> SortHalfEdges(List<HalfEdge> halfedges, Polygon polygon) {

		//Find center point of polygon
		Vector3 center = Vector3.zero;
		float num = 0;
		foreach (HalfEdge h in halfedges) {
			h.poly = polygon;
			center+= h.start;
			num++;
		}
		center = center / num;

		halfedges.Sort((p1, p2) => Math.Sign(Vector3.Dot(polygon.plane.normal, Vector3.Cross(p1.start - center, p2.start - center))));
	
		return halfedges;
	}

	//INCOMPLETE REFACTORING?
	/*
	public static List<HalfEdge> SortHalfEdges(List<HalfEdge> halfedges, Polygon p) {
		
		//Order vertices in counterclockwise direction according to right hand rule

		//Find center point of polygon
		Vector3 avg = Vector3.zero;
		float num = 0;
		foreach (HalfEdge h in halfedges) {
			h.poly = p;
			avg += h.start;
			num++;
		}
		avg = avg / num;

		//Compare cross product of points relative to normal
		List<HalfEdge> sortedHalfEdges = new List<HalfEdge>();
		sortedHalfEdges.Add(halfedges[0]);

		/////////////////////////////////////////////////////////////////////
		//REFACTOR THIS CODE?

		for (int i = 1; i < halfedges.Count; i++) {

			//Determine if each stored vertex is before or after p.verts[i] 
			//(before = 0, after = 1)
			int[] bin = new int[sortedHalfEdges.Count];
			for (int j = 0; j < sortedHalfEdges.Count; j++) {
				Vector3 cross = Vector3.Cross(sortedHalfEdges[j].start - avg, halfedges[i].start - avg);
				if (Vector3.Dot(p.plane.normal, cross) < 0) {
					bin[j] = 0;
				} else {
					bin[j] = 1;
				}
			}

			//Parse binary indices and insert p.verts[i] accordingly
			//All 0 - Add to End
			//All 1 - Insert at Front
			//Else - Insert at Leading 1 (First 1 after a 0)

			bool uniform = true; //All 0 or All 1?
			for (int k = 1; k < bin.Length; k++) {
				if (bin[k - 1] != bin[k]) {
					uniform = false;
					if (bin[k] == 1) { //Leading 1 in List
						sortedHalfEdges.Insert(k, halfedges[i]);
					}
					break;
				}
			}

			if (uniform) {
				if (bin[0] == 0) { //All 0
					sortedHalfEdges.Add(halfedges[i]);
				} else { //All 1
					sortedHalfEdges.Insert(0, halfedges[i]);
				}
			} else { //Leading 1 at Front
				if (sortedHalfEdges.IndexOf(halfedges[i]) == -1 && bin[bin.Length - 1] == 0) {
					sortedHalfEdges.Insert(0, halfedges[i]);
				}
			}
		}

		return sortedHalfEdges;
	}
	//*/

	public static void BindSerialHalfEdges(List<Polygon> polys) {
		
		foreach (Polygon p in polys) {

			// Last HalfEdge points to first
			p.hedges[p.hedges.Count - 1].next = p.hedges[0]; 

			// Bind successive HalfEdges
			for (int i = 0; i < p.hedges.Count - 1; i++) {
				p.hedges[i].next = p.hedges[i + 1];
			}
		}
	}

	public static void BindAntiparallelHalfEdges(List<Polygon> polys) {

		// Compile all HalfEdges into list
		List<HalfEdge> brushHEs = new List<HalfEdge>();
		foreach (Polygon p in polys) {
			brushHEs.AddRange(p.hedges);
		}

		// For each HalfEdge, if twin is unbound (null), find pair in list
		for (int i = 0; i < brushHEs.Count; i++) {
			if (brushHEs[i].twin == null) {
				for (int j = i + 1; j < brushHEs.Count; j++) {
					if (brushHEs[i].start == brushHEs[j].next.start &&
						brushHEs[i].next.start == brushHEs[j].start) {
						brushHEs[i].twin = brushHEs[j];
						brushHEs[j].twin = brushHEs[i];
						brushHEs.RemoveAt(j);
						break;
					}
				}
			}
		}
	}
	//*/

	// Calculations
	// Local Space?
	// Compare Planes
	// Cull Vertices
}
