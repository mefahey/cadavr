﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Polygon {

	public List<HalfEdge> hedges;
	public Plane plane;

	public Material material;

	public Polygon(Plane p) {
		hedges = new List<HalfEdge>();
		plane = p;
		material = null;
	}

	public Polygon(Plane p, Material m) {
		hedges = new List<HalfEdge>();
		plane = p;
		material = m;
	}

	public Polygon(List<HalfEdge> h, Plane p) {
		hedges = h;
		plane = p;
		material = null;
	}

	public Polygon(List<HalfEdge> h, Plane p, Material m) {
		hedges = h;
		plane = p;
		material = m;
	}
}

public class HalfEdge {

	public Vector3 start;
	public HalfEdge twin;
	public HalfEdge next;
	public Polygon poly;

	public HalfEdge(Vector3 s) {
		start = s;
		twin = null;
		next = null;
		poly = null;
	}

	public HalfEdge(Vector3 s, Polygon p) {
		start = s;
		twin = null;
		next = null;
		poly = p;
	}
}

public class Surface {

	public List<Polygon> polys;

	public Surface(List<Polygon> p) {
		polys = p;
	}
	
}

// ------------------- //
public abstract class Brush {

	//public List<Surface> surfaces;
	public Dictionary<GameObject, List<Surface>> surfaces;

	public GameObject gameObject; 
	//empty parent -> stores location/orientation
	//apply scale to surfaces before cutting (handle subsurfaces as necessary)

	//Shared Functionality?
}

public class PrimitiveBrush : Brush {

	//Add serializable fields to subclasses
	//Cube    -> side length
	//Prism   -> height, inscribed radius, base shape (3 to 12?)
	//Sphere  -> Radius, LOD
	//Pyramid -> height, inscribed radius, base shape (3 to 12?)

	public PrimitiveBrush(Surface surface, GameObject gameObject) {
		//this.surfaces = new List<Surface> { surface };
		surfaces = new Dictionary<GameObject, List<Surface>>();
		surfaces[gameObject] = new List<Surface> { surface };

		//Set as empty parent for consistency with DerivativeBrush?
		this.gameObject = gameObject; //pass in cube/cylinder/etc.
		//gameObject.name = "PrimitiveBrush";
	}
}

public enum Operation { AorB, AnotB, BnotA, AandB };

public class DerivativeBrush : Brush {

	private Dictionary<GameObject, List<Surface>>[] interiors = new Dictionary<GameObject, List<Surface>>[2];
	private Dictionary<GameObject, List<Surface>>[] exteriors = new Dictionary<GameObject, List<Surface>>[2];

	public Operation operation;

	public DerivativeBrush(Brush a, Brush b, Operation operation) {

		//this.a = a;
		//this.b = b;
		this.operation = operation;

		//Execute splits, populate interiors and exteriors
		//CSGBrushComparator.splitBrushByBrush(a, b, out interiors[0], out exteriors[0]);
		//CSGBrushComparator.splitBrushByBrush(b, a, out interiors[1], out exteriors[1]);

		//NOTE: MULTISURFACE CUTTING HAS STOPPED WORKING -> FIX
		//CAUSE: INCLUDING ALL OF A.SURFACES AND B.SURFACES IN AORB RESULTS IN OVERLPAP DURING SUBSEQUENT SUBTRACTION

		Debug.Log("SPLIT SUCCESSFUL");

		surfaces = new Dictionary<GameObject, List<Surface>>();
		gameObject = new GameObject();
		gameObject.name = "DerivativeBrush";
		//create function to set gameObject properties?
		//(AnotB -> A, BnotA -> B, AorB/AandB -> Average of A and B)

		//Apply operation, populate surfaces accordingly
		ApplyOperation(a, b);
	}

	// Call ApplyOperation on change of operation 
		// NOTE: ApplyOperation cannot take Brush parameters (not stored) -> Needs fixed
	// Call new DerivativeBrush on change of Brush a or b

	private void ApplyOperation(Brush a, Brush b) {

		surfaces.Clear();

		if (operation == Operation.AorB) {
			surfaces = new Dictionary<GameObject, List<Surface>>();
			a.surfaces.ToList().ForEach(x => surfaces.Add(x.Key, x.Value));
			b.surfaces.ToList().ForEach(x => surfaces.Add(x.Key, x.Value));
		} else if (operation == Operation.AnotB) {
			surfaces = exteriors[0];
		} else if (operation == Operation.BnotA) {
			surfaces = exteriors[1];
		} else if (operation == Operation.AandB) {
			surfaces = interiors[0]; 
			//Add conditional to use mesh with fewer gameObjects?
		}

		GenerateGameObjects(surfaces);
	}

	private void GenerateGameObjects(Dictionary<GameObject, List<Surface>> surfaces) {

		List<GameObject> gameObjects = surfaces.Keys.ToList();
		foreach (GameObject g in gameObjects) {
			CompileGameObjects(surfaces[g], g);
		}
	}

	// NOTE: Renders unnecessary internal faces for all operations except Intersect
	private void CompileGameObjects(List<Surface> subsurfaces, GameObject reference) {

		Dictionary<Material, List<Polygon>> dictionary = new Dictionary<Material, List<Polygon>>();

		//Exclude internal polygons unless s in last surface in surfaces?
		//How to differentiate between external and internal?
		foreach (Surface s in subsurfaces) {
			foreach (Polygon p in s.polys) {
				if (!dictionary.ContainsKey(p.material)) {
					dictionary[p.material] = new List<Polygon>();
				}
				dictionary[p.material].Add(p);
			}
		}

		GameObject g = reference;

		foreach (Material m in dictionary.Keys) {
			g = new GameObject();

			g.AddComponent<MeshRenderer>();
			g.GetComponent<MeshRenderer>().material = m;

			g.AddComponent<MeshFilter>();
			g.GetComponent<MeshFilter>().mesh = CSGBrushComparator.GenerateMesh(subsurfaces, m);

			g.transform.position = reference.transform.position;
			g.transform.rotation = reference.transform.rotation;
			g.transform.localScale = reference.transform.localScale;
			g.transform.SetParent(gameObject.transform);
		}

		//Rebind surfaces to one of the generated gameObjects (child of Brush gameObject)
		surfaces[g] = surfaces[reference];
		surfaces.Remove(reference);
	}
}



//Vertex 

//Edge

//Polygon
//Use AABB to check if polygons intersect? (bounding box)
//Test vertices of polygon with raycasting (even, odd)

//bool Intersects(Polygon other) (?)

/*
Notes

Categories (Touching, Outside, Inside) are separate list entities, not tags
Subdivide brushes into distinct surfaces with independent materials

*/
